package config

import "log"

const (
	GoWUnknown = iota
	GoW1
	GoW2
)

const (
	PS2 = iota
	PS3
	PSVita
)

type GoWVersion int
type PSVersion int

var gowVersion GoWVersion = GoWUnknown
var psVersion PSVersion = PS2

func GetGoWVersion() GoWVersion {
	return gowVersion
}

func SetGoWVersion(version GoWVersion) {
	switch version {
	default:
		log.Panicf("Unknown game version '%v'", version)
	case GoWUnknown:
	case GoW1:
	case GoW2:
	}
	gowVersion = version
}

func GetPSVersion() PSVersion {
	return psVersion
}

func SetPSVersion(version PSVersion) {
	psVersion = version
}
