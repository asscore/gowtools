package iso

import (
	"encoding/binary"
	"errors"
	"io"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/asscore2/udf"
	"gowtools/utils"
)

func Copy(isoFile string, gameFolder string) error {
	var layers [2]*udf.Udf
	var secondLayerStart int64 = 0

	fi, err := os.Open(isoFile)
	if err != nil {
		return err
	}
	//noinspection GoUnhandledErrorResult
	defer fi.Close()

	layers[0] = udf.NewUdfFromReader(fi)

	var volSizeBuf [4]byte

	size := func() int64 {
		if stat, err := os.Stat(isoFile); err != nil {
			return 0
		} else {
			return stat.Size()
		}
	}

	if _, err := fi.ReadAt(volSizeBuf[:], 0x10*2048+80); err != nil {
		log.Printf("Error when detecting second layer: %v", err)
	} else {
		volumeSize := int64(binary.LittleEndian.Uint32(volSizeBuf[:])-16) * utils.SectorSize
		if volumeSize+32*utils.SectorSize < size() {
			layers[1] = udf.NewUdfFromReader(io.NewSectionReader(fi, volumeSize, size()-volumeSize))
			secondLayerStart = volumeSize
		}
	}

	if err := os.MkdirAll(gameFolder, 0666); err != nil {
		return err
	}

	re := regexp.MustCompile(`^GODOFWAR[.]TOC|PART[1-2][.]PAK$`)

	for c, layer := range layers {
		if layer != nil {
			dir := layer.ReadDir(nil)
			for i := range dir {
				if re.MatchString(strings.ToUpper(dir[i].Name())) {
					fo, err := os.Create(filepath.Join(gameFolder, strings.ToUpper(dir[i].Name())))
					if err != nil {
						return err
					}
					//noinspection GoDeferInLoop, GoUnhandledErrorResult
					defer fo.Close()

					offset := dir[i].GetFileOffset()
					if c == 1 {
						offset += secondLayerStart
					}

					if _, err := fi.Seek(offset, io.SeekStart); err != nil {
						log.Fatalf("Error when seeking: %v", err)
					}

					log.Printf("Copying (s:%v) %s\n", dir[i].Size(), strings.ToUpper(dir[i].Name()))

					written, err := io.CopyN(fo, fi, dir[i].Size())
					if err != nil && err != io.EOF {
						return err
					} else if written != dir[i].Size() {
						return errors.New("file not copied fully")
					}
					//noinspection GoUnhandledErrorResult
					fo.Close()
				}
			}
		}
	}

	return nil
}
