package wad

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"path/filepath"
	"strings"

	"gowtools/config"
	"gowtools/utils"
)

const (
	NodeTypeData = iota
	NodeTypeLink
)

//noinspection GoNameStartsWithPackageName
type WadNode struct {
	Name     string // może być pusta
	Path     string
	Parent   *WadNode
	Wad      *Wad
	Type     int // NodeType*
	SubNodes []*WadNode
	Depth    int

	// NodeTypeData
	Size      uint32
	Format    uint32 // pierwsze 4 bajty danych
	DataStart uint32

	// cache'owanie eksportu danych
	Extracted      bool
	Cache          interface{}
	ExtractedNames []string

	// NodeTypeLink
	LinkTo *WadNode
}

type Wad struct {
	Nodes  []*WadNode
	Reader io.ReaderAt

	Name    string
	Version config.GoWVersion
}

//noinspection GoNameStartsWithPackageName
type WadFormatExporter interface {
	ExtractFromNode(wadNode *WadNode, outName string) error
}

var wadExporter = make(map[uint32]WadFormatExporter, 0)

func PregisterExporter(formatMagic uint32, exporter WadFormatExporter) {
	wadExporter[formatMagic] = exporter
}

func (nd *WadNode) StringPrefixed(prefix string) string {
	switch nd.Type {
	case NodeTypeData:
		res := fmt.Sprintf("%sdata size: 0x%.6x format: 0x%.8x start: 0x%.8x '%s'",
			prefix, nd.Size, nd.Format, nd.DataStart, nd.Name)

		if len(nd.SubNodes) > 0 {
			postfix := prefix + "  "
			res += " {\n"

			for _, n := range nd.SubNodes {
				res += fmt.Sprintf("%s\n", n.StringPrefixed(postfix))
			}
			res = fmt.Sprintf("%s%s}", res, prefix)
		}
		return res
	case NodeTypeLink:
		if nd.LinkTo != nil {
			return fmt.Sprintf("%slink '%s' -> '%s'", prefix, nd.Name, nd.LinkTo.Path)
		} else {
			return fmt.Sprintf("%slink '%s' #UNRESOLVED_LINK#", prefix, nd.Name)
		}
	}

	return prefix + "! ! ! ! unknown node type\n"
}

func (nd *WadNode) String() string {
	return nd.StringPrefixed("")
}

func (nd *WadNode) Find(name string, uptree bool) *WadNode {
	for _, v := range nd.SubNodes {
		if v.Name == name {
			return v
		}
	}
	if uptree {
		if nd.Parent != nil {
			return nd.Parent.Find(name, uptree)
		} else {
			return nd.Wad.Find(name)
		}
	}

	return nil
}

func (nd *WadNode) DataReader() (*io.SectionReader, error) {
	if nd.Type != NodeTypeData {
		return nil, errors.New("node must be data for reading")
	} else {
		return io.NewSectionReader(nd.Wad.Reader, int64(nd.DataStart), int64(nd.Size)), nil
	}
}

func (nd *WadNode) DataRead() ([]byte, error) {
	rdr, err := nd.DataReader()
	if err != nil {
		return nil, err
	}

	buf := make([]byte, nd.Size)
	if _, err = rdr.ReadAt(buf, 0); err != nil {
		return nil, err
	} else {
		return buf, nil
	}
}

func (nd *WadNode) Extract(outFolder string, dump bool) error {
	if nd.Type == NodeTypeData {
		path := filepath.Join(outFolder, nd.Wad.Name, strings.Replace(nd.Path, ":", "-", -1))

		for _, sn := range nd.SubNodes {
			if err := sn.Extract(outFolder, dump); err != nil {
				return err
			}
		}

		if !nd.Extracted {
			if dump {
				name := path + ".dump"

				rdr, derr := nd.DataReader()
				if derr == nil {
					if derr = os.MkdirAll(filepath.Dir(name), 0777); derr == nil {
						var f *os.File
						f, derr = os.Create(name)
						if derr == nil {
							//noinspection GoUnhandledErrorResult
							defer f.Close()
							_, derr = io.Copy(f, rdr)
						}
					}
				}
				if derr != nil {
					return fmt.Errorf("error when dumping '%s' -> '%s': %v", nd.Path, name, derr)
				}
			}

			if ex, f := wadExporter[nd.Format]; f {
				if err := ex.ExtractFromNode(nd, path); err != nil {
					return fmt.Errorf("error when extracting '%s': %v", nd.Path, err)
				}
			}
			nd.Extracted = true
		}
	}

	return nil
}

func (wad *Wad) Find(name string) *WadNode {
	for _, v := range wad.Nodes {
		if v.Name == name {
			return v
		}
	}

	return nil
}

func (wad *Wad) Extract(outFolder string, dump bool) error {
	for _, nd := range wad.Nodes {
		if err := nd.Extract(outFolder, dump); err != nil {
			return err
		}
	}

	return nil
}

func (wad *Wad) NewNode(parent *WadNode, name string, nodeType int) *WadNode {
	node := &WadNode{
		Parent: parent,
		Type:   nodeType,
		Wad:    wad,
		Name:   name,
	}

	node.Depth = 0
	if parent != nil {
		node.Depth = parent.Depth + 1
	}
	node.Path = name
	if node.Parent != nil {
		node.Path = filepath.Join(node.Parent.Path, node.Path)
	}

	return node
}

func (wad *Wad) DetectVersion() (config.GoWVersion, error) {
	wad.Version = config.GoWUnknown

	var buffer [4]byte
	if _, err := wad.Reader.ReadAt(buffer[:], 0); err != nil {
		return wad.Version, err
	}

	firstTag := binary.LittleEndian.Uint32(buffer[:])
	switch firstTag {
	case 0x378:
		wad.Version = config.GoW1
	case 0x15:
		wad.Version = config.GoW2
	default:
		return wad.Version, errors.New("cannot detect version")
	}

	config.SetGoWVersion(wad.Version)

	return wad.Version, nil
}

func NewWad(f io.ReaderAt, name string, version config.GoWVersion) (*Wad, error) {
	wad := &Wad{
		Reader:  f,
		Name:    name,
		Version: version,
	}

	if version == config.GoWUnknown {
		var err error
		wad.Version, err = wad.DetectVersion()
		if err != nil {
			return nil, err
		}
		log.Printf("Detected WAD version: %v\n", wad.Version)
	}

	item := make([]byte, 0x20)
	newGroupTag := false
	var currentNode *WadNode

	pos := int64(0)

	for {
		const (
			PackStuff = iota
			PackGroupStart
			PackGroupEnd
			PackData
		)

		packId := PackStuff

		dataPos := pos + 0x20
		n, err := f.ReadAt(item, pos)
		if err != nil {
			if err == io.EOF {
				if n != 0x20 && n != 0 {
					return nil, errors.New("file end is corrupt")
				} else {
					break
				}
			} else {
				return nil, err
			}
		}

		tag := binary.LittleEndian.Uint16(item[:2])
		size := binary.LittleEndian.Uint32(item[4:8])
		name := utils.BytesToString(item[8:32])

		switch wad.Version {
		case config.GoW2:
			const (
				TagFileDataPacket     = 0x01
				TagFileDataGroupStart = 0x02
				TagFileDataGroupEnd   = 0x03
				TagFileHeaderStart    = 0x15
				TagFileHeaderPopHeap  = 0x16
				TagFileDataStart      = 0x13
				TagEntityCount        = 0x0
			)

			switch tag {
			case TagFileDataPacket:
				packId = PackData
			case TagFileDataGroupStart:
				packId = PackGroupStart
			case TagFileDataGroupEnd:
				packId = PackGroupEnd
			case TagFileDataStart:
				packId = PackStuff
			case TagFileHeaderStart:
				packId = PackStuff
			case TagFileHeaderPopHeap:
				packId = PackStuff
			case TagEntityCount:
				size = 0
				packId = PackStuff
			}
		case config.GoW1:
			const (
				TagFileDataPacket     = 0x1e
				TagFileDataGroupStart = 0x28
				TagFileDataGroupEnd   = 0x32
				TagFileHeaderStart    = 0x378
				TagFileHeaderPopHeap  = 0x3e7
				TagFileDataStart      = 0x29a
				TagEntityCount        = 0x18
			)

			switch tag {
			case TagFileDataPacket:
				packId = PackData
			case TagFileDataGroupStart:
				packId = PackGroupStart
			case TagFileDataGroupEnd:
				packId = PackGroupEnd
			case TagFileHeaderStart:
				packId = PackStuff
			case TagFileHeaderPopHeap:
				packId = PackStuff
			case TagFileDataStart:
				packId = PackStuff
			case TagEntityCount:
				size = 0
				packId = PackStuff
			}
		}

		switch packId {
		case PackGroupEnd:
			newGroupTag = false
			if currentNode == nil {
				return nil, errors.New("trying to end of not started group")
			} else {
				currentNode = currentNode.Parent
			}
		case PackGroupStart:
			newGroupTag = true
		case PackData:
			var node *WadNode
			if size == 0 {
				node = wad.NewNode(currentNode, name, NodeTypeLink)
				if currentNode == nil {
					return nil, errors.New("link cannot be in root node")
				}
				for cn := currentNode; cn != nil && node.LinkTo == nil; cn = cn.Parent {
					for _, v := range cn.SubNodes {
						if v.Name == node.Name {
							node.LinkTo = v
						}
					}
				}
				if node.LinkTo == nil {
					for _, v := range wad.Nodes {
						if v.Name == node.Name {
							node.LinkTo = v
						}
					}
				}
				if node.LinkTo == nil {
					log.Printf("Unresolved link to '%s'", node.Name)
				}
			} else {
				node = wad.NewNode(currentNode, name, NodeTypeData)
				var bfmt [4]byte
				if _, err := wad.Reader.ReadAt(bfmt[:], dataPos); err != nil {
					return nil, err
				}
				node.Format = binary.LittleEndian.Uint32(bfmt[:4])
			}
			node.Size = size
			node.DataStart = uint32(dataPos)

			if currentNode == nil {
				wad.Nodes = append(wad.Nodes, node)
			} else {
				currentNode.SubNodes = append(currentNode.SubNodes, node)
			}

			if newGroupTag {
				newGroupTag = false
				currentNode = node
			}
		case PackStuff:
		}

		off := (size + 15) & (15 ^ math.MaxUint32)
		pos = int64(off) + pos + 0x20
	}

	return wad, nil
}
