package export

import _ "gowtools/files/wad/export/gfx"
import _ "gowtools/files/wad/export/mat"
import _ "gowtools/files/wad/export/mdl"
import _ "gowtools/files/wad/export/mesh"
import _ "gowtools/files/wad/export/obj"
import _ "gowtools/files/wad/export/sbk"
import _ "gowtools/files/wad/export/txr"
