package obj

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"

	"gowtools/files/wad"
	"gowtools/files/wad/export/mdl"
	"gowtools/utils"
)

type Joint struct {
	Id          int16
	Name        string
	ChildsStart int16
	ChildsEnd   int16
	Parent      int16

	IsSkinned bool
	InvId     int16

	BindToJointMat utils.Mat4
	ParentToJoint  utils.Mat4

	OurJointToIdleMat utils.Mat4
	RenderMat         utils.Mat4
}

const JointChildNone = -1

type Object struct {
	Joints []Joint

	JointsCount uint32

	Matrixes1 []utils.Mat4
	Matrixes3 []utils.Mat4
}

const ObjectMagic = 0x00040001
const HeaderSize = 0x2c
const DataHeaderSize = 0x30

func init() {
	wad.PregisterExporter(ObjectMagic, &Object{})
}

func (obj *Object) StringJoint(id int16, spaces string) string {
	j := obj.Joints[id]
	return fmt.Sprintf("[%.4x]%s %s\n", j.Id, spaces, j.Name)
}

func (obj *Object) StringTree() string {
	stack := make([]int16, 0, 32)
	spaces := string(make([]byte, 0, 64))
	spaces = ""

	var buffer bytes.Buffer

	for i := int16(0); i < int16(obj.JointsCount); i++ {
		j := obj.Joints[i]

		if j.Parent != JointChildNone {
			for i == stack[len(stack)-1] {
				stack = stack[:len(stack)-1]
				spaces = spaces[:len(spaces)-2]
			}
		}

		buffer.WriteString(obj.StringJoint(i, spaces))

		if j.ChildsStart != JointChildNone {
			if j.ChildsEnd == -1 && len(stack) > 0 {
				stack = append(stack, stack[len(stack)-1])
			} else {
				stack = append(stack, j.ChildsEnd)
			}
			spaces += " -"
		}
	}
	return buffer.String()
}

func NewFromData(fobj io.Reader) (*Object, error) {
	file, err := ioutil.ReadAll(fobj)
	if err != nil {
		return nil, err
	}

	obj := new(Object)

	obj.JointsCount = binary.LittleEndian.Uint32(file[0x1c:0x20])
	obj.Joints = make([]Joint, obj.JointsCount)

	dataOffset := binary.LittleEndian.Uint32(file[0x28:0x2c])
	matData := file[dataOffset : dataOffset+DataHeaderSize]

	mat1Count := binary.LittleEndian.Uint32(matData[:4])
	mat3Offset := binary.LittleEndian.Uint32(matData[12:16])
	mat3Count := binary.LittleEndian.Uint32(matData[16:20])

	invId := int16(0)
	for i := range obj.Joints {
		jointBufStart := HeaderSize + i*0x10
		jointBuf := file[jointBufStart : jointBufStart+0x10]

		nameBufStart := HeaderSize + int(obj.JointsCount)*0x10 + i*0x18
		nameBuf := file[nameBufStart : nameBufStart+0x18]

		flags := binary.LittleEndian.Uint32(jointBuf[:4])

		isInvMat := flags&0xa0 == 0xa0 || obj.JointsCount == mat3Count
		obj.Joints[i] = Joint{
			Name:        utils.BytesToString(nameBuf[:]),
			ChildsStart: int16(binary.LittleEndian.Uint16(jointBuf[0x4:0x6])),
			ChildsEnd:   int16(binary.LittleEndian.Uint16(jointBuf[0x6:0x8])),
			Parent:      int16(binary.LittleEndian.Uint16(jointBuf[0x8:0xa])),
			Id:          int16(i),
			IsSkinned:   isInvMat,
			InvId:       invId,
		}

		if isInvMat {
			invId++
		}
	}

	obj.Matrixes1 = make([]utils.Mat4, mat1Count)
	obj.Matrixes3 = make([]utils.Mat4, mat3Count)

	mat1Buf := file[dataOffset+DataHeaderSize : dataOffset+DataHeaderSize+uint32(len(obj.Matrixes1))*0x40]
	mat3Buf := file[dataOffset+mat3Offset : dataOffset+mat3Offset+uint32(len(obj.Matrixes3))*0x40]

	for i := range obj.Matrixes1 {
		if err := binary.Read(bytes.NewReader(mat1Buf[i*0x40:i*0x40+0x40]), binary.LittleEndian, &obj.Matrixes1[i]); err != nil {
			return nil, err
		}
	}
	for i := range obj.Matrixes3 {
		if err := binary.Read(bytes.NewReader(mat3Buf[i*0x40:i*0x40+0x40]), binary.LittleEndian, &obj.Matrixes3[i]); err != nil {
			return nil, err
		}
	}

	obj.FeelJoints()

	log.Printf("Joints tree:\n%s", obj.StringTree())

	return obj, nil
}

func (obj *Object) FeelJoints() {
	for i := range obj.Joints {
		j := &obj.Joints[i]
		j.ParentToJoint = obj.Matrixes1[i]

		if j.IsSkinned {
			j.BindToJointMat = obj.Matrixes3[j.InvId]
		} else {
			j.BindToJointMat = utils.Ident4()
		}

		j.OurJointToIdleMat = j.ParentToJoint
		if j.Parent != JointChildNone {
			j.OurJointToIdleMat = obj.Joints[j.Parent].OurJointToIdleMat.Mul4(j.ParentToJoint)
		}

		if j.IsSkinned {
			j.RenderMat = j.OurJointToIdleMat.Mul4(j.BindToJointMat)
		} else {
			j.RenderMat = j.OurJointToIdleMat
		}
	}
}

func (*Object) ExtractFromNode(nd *wad.WadNode, outName string) error {
	log.Printf("Object '%s' extraction", nd.Path)
	reader, err := nd.DataReader()
	if err != nil {
		return err
	}

	obj, err := NewFromData(reader)
	if err != nil {
		return err
	}

	nd.Cache = obj

	for _, n := range nd.SubNodes {
		if n.Type == wad.NodeTypeLink {
			n = n.LinkTo
		}
		if n.Format == mdl.ModelMagic {
			if !n.Extracted || n.Cache == nil {
				return errors.New("model not loaded before object")
			} else {
				m := n.Cache.(*mdl.Model)
				bones := make([]utils.Mat4, m.JointsCount)
				for i, joint := range obj.Joints {
					bones[i] = joint.RenderMat
				}
				names, err := m.Extract(n, bones, outName)
				if err != nil {
					return err
				}
				nd.ExtractedNames = names
				log.Printf("Object '%s' extracted: %s", nd.Path, names)
			}
		}
	}

	return nil
}
