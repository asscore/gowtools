package gfx

import (
	"encoding/binary"
	"errors"
	"fmt"
	"image/color"
	"io"
	"log"
	"math"

	"gowtools/config"
	"gowtools/files/wad"
)

//noinspection GoNameStartsWithPackageName
const GfxMagic = 0x0000000c
const HeaderSize = 0x18

type Gfx struct {
	Width      uint32
	Height     uint32
	RealHeight uint32
	Encoding   uint32
	Bpi        uint32
	DataSize   uint32
	Data       [][]byte
}

func init() {
	wad.PregisterExporter(GfxMagic, &Gfx{})
}

func IndexSwizzlePalette(i int) int {
	remap := []int{0, 2, 1, 3}
	blockId := i / 8
	blockPos := i % 8

	return blockPos + (remap[blockId%4]+(blockId/4)*4)*8
}

func (gfx *Gfx) AsRawPalette(idx int) ([]uint32, error) {
	palbuf := gfx.Data[idx]
	colors := gfx.Width * gfx.RealHeight

	palette := make([]uint32, colors)
	for i := range palette {
		clr := binary.LittleEndian.Uint32(palbuf[i*4 : i*4+4])
		switch gfx.Height {
		case 2:
			palette[i] = clr
		case 32:
			fallthrough
		case 16:
			palette[IndexSwizzlePalette(i)] = clr
		default:
			return nil, fmt.Errorf("wrong palette height: %d", gfx.Height)
		}
	}

	return palette, nil
}

func (gfx *Gfx) AsPalette(idx int, convertAlphaToPCformat bool) ([]color.NRGBA, error) {
	rawPal, err := gfx.AsRawPalette(idx)
	if err != nil {
		return nil, err
	}

	palette := make([]color.NRGBA, len(rawPal))
	for i, raw := range rawPal {
		clr := color.NRGBA{
			R: uint8(raw),
			G: uint8(raw >> 8),
			B: uint8(raw >> 16),
			A: uint8(raw >> 24),
		}
		if convertAlphaToPCformat {
			clr.A = uint8(float32(clr.A) * (255.0 / 128.0))
		}
		palette[i] = clr
	}

	return palette, nil
}

func IndexUnswizzleTexture(x, y, width uint32) uint32 {
	blockLocation := (y&(math.MaxUint32^0xf))*width + (x&(math.MaxUint32^0xf))*2
	swapSelector := (((y + 2) >> 2) & 0x1) * 4
	posY := (((y & (math.MaxUint32 ^ 3)) >> 1) + (y & 1)) & 0x7
	columnLocation := posY*width*2 + ((x+swapSelector)&0x7)*4

	byteNum := ((y >> 1) & 1) + ((x >> 2) & 2)
	return blockLocation + columnLocation + byteNum
}

func (gfx *Gfx) AsPaletteIndexes(idx int) []byte {
	data := gfx.Data[idx]

	indexes := make([]byte, gfx.Width*gfx.RealHeight)
	switch gfx.Bpi {
	case 8:
		for y := uint32(0); y < gfx.RealHeight; y++ {
			for x := uint32(0); x < gfx.Width; x++ {
				if gfx.Encoding&2 == 0 {
					pos := IndexUnswizzleTexture(x, y, gfx.Width)
					if pos < uint32(len(data)) {
						indexes[x+y*gfx.Width] = data[pos]
					} else {
						log.Printf("Warning: Texture missed var: len=%v < pos=%v, x=%v, y=%v. w=%v h=%v",
							len(data), pos, x, y, gfx.Width, gfx.Height)
					}
				} else {
					indexes[x+y*gfx.Width] = data[x+y*gfx.Width]
				}
			}
		}
	case 4:
		for y := uint32(0); y < gfx.RealHeight; y++ {
			for x := uint32(0); x < gfx.Width; x++ {
				val := data[(x+y*gfx.Width)/2]
				if x&1 == 0 {
					indexes[x+y*gfx.Width] = val & 0xf
				} else {
					indexes[x+y*gfx.Width] = val >> 4
				}
			}
		}
	default:
		panic("Unknown palette indexes encoding case")
	}

	return indexes
}

func (gfx *Gfx) String() string {
	return fmt.Sprintf("Gfx Width: %d Height: %d RealHeight: %d Bpi: %d Encoding: %d Datas: %d\n",
		gfx.Width, gfx.Height, gfx.RealHeight, gfx.Bpi, gfx.Encoding, len(gfx.Data))
}

func NewFromData(fgfx io.ReaderAt) (*Gfx, error) {
	buf := make([]byte, HeaderSize)
	if _, err := fgfx.ReadAt(buf, 0); err != nil {
		return nil, err
	}

	magic := binary.LittleEndian.Uint32(buf[:4])
	if magic != GfxMagic {
		return nil, errors.New("wrong magic")
	}

	gfx := &Gfx{
		Width:    binary.LittleEndian.Uint32(buf[4:8]),
		Height:   binary.LittleEndian.Uint32(buf[8:12]),
		Encoding: binary.LittleEndian.Uint32(buf[12:16]),
		Bpi:      binary.LittleEndian.Uint32(buf[16:20]),
		Data:     make([][]byte, binary.LittleEndian.Uint32(buf[20:24])),
	}

	gfx.RealHeight = gfx.Height / uint32(len(gfx.Data))
	gfx.DataSize = ((gfx.Width * gfx.RealHeight) * gfx.Bpi) / 8

	if config.GetPSVersion() == config.PS2 {
		pos := uint32(HeaderSize)
		for iData := range gfx.Data {
			rawData := make([]byte, gfx.DataSize)
			if _, err := fgfx.ReadAt(rawData, int64(pos)); err != nil {
				return nil, err
			}
			gfx.Data[iData] = rawData
			pos += gfx.DataSize
		}
	} else {
		gfx.Data = nil
	}

	return gfx, nil
}

//noinspection GoUnusedParameter
func (*Gfx) ExtractFromNode(nd *wad.WadNode, outName string) error {
	log.Printf("Gfx '%s' extraction", nd.Path)
	reader, err := nd.DataReader()
	if err != nil {
		return err
	}

	mat, err := NewFromData(reader)
	if err != nil {
		return err
	}

	nd.Cache = mat

	return nil
}
