package txr

import (
	"encoding/binary"
	"errors"
	"fmt"
	"image"
	"image/png"
	"io"
	"log"
	"os"
	"path/filepath"

	"gowtools/files/wad"
	fgfx "gowtools/files/wad/export/gfx"
	"gowtools/utils"
)

type Texture struct {
	GfxName    string
	PalName    string
	SubTxrName string
}

const TextureMagic = 0x00000007
const FileSize = 0x58

func init() {
	wad.PregisterExporter(TextureMagic, &Texture{})
}

func NewFromData(fin io.ReaderAt) (*Texture, error) {
	buf := make([]byte, FileSize)
	if _, err := fin.ReadAt(buf, 0); err != nil {
		return nil, err
	}

	magic := binary.LittleEndian.Uint32(buf[:4])
	if magic != TextureMagic {
		return nil, errors.New("wrong magic")
	}

	tex := &Texture{
		GfxName:    utils.BytesToString(buf[4:28]),
		PalName:    utils.BytesToString(buf[28:52]),
		SubTxrName: utils.BytesToString(buf[52:76]),
	}

	return tex, nil
}

func (txr *Texture) Image(gfx *fgfx.Gfx, pal *fgfx.Gfx, igfx int, ipal int) (image.Image, error) {
	width := int(gfx.Width)
	height := int(gfx.RealHeight)

	img := image.NewRGBA(image.Rect(0, 0, width, height))
	palette, err := pal.AsPalette(ipal, true)

	if err != nil {
		return nil, err
	}

	palIdx := gfx.AsPaletteIndexes(igfx)

	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			img.Set(x, y, palette[palIdx[x+y*width]])
		}
	}

	return img, nil
}

func (txr *Texture) Extract(gfx *fgfx.Gfx, pal *fgfx.Gfx, outName string) ([]string, error) {
	names := make([]string, 0)
	for iGfx := range gfx.Data {
		for iPal := range pal.Data {
			img, err := txr.Image(gfx, pal, iGfx, iPal)
			if err != nil {
				return nil, err
			}

			var resultFileName string
			if iGfx == 0 && iPal == 0 {
				resultFileName = outName + ".png"
			} else {
				resultFileName = fmt.Sprintf("%s.%d.%d.png", outName, iGfx, iPal)
			}

			if err = os.MkdirAll(filepath.Dir(resultFileName), 0777); err != nil {
				return nil, err
			}

			fof, err := os.Create(resultFileName)
			if err != nil {
				return nil, err
			}
			//noinspection GoDeferInLoop, GoUnhandledErrorResult
			defer fof.Close()

			if err = png.Encode(fof, img); err != nil {
				return nil, err
			}

			names = append(names, resultFileName)
		}
	}

	return names, nil
}

func (*Texture) ExtractFromNode(nd *wad.WadNode, outName string) error {
	reader, err := nd.DataReader()
	if err != nil {
		return err
	}

	txr, err := NewFromData(reader)
	if err != nil {
		return err
	}

	if txr.GfxName != "" && txr.PalName != "" {
		gfxnd := nd.Find(txr.GfxName, true)
		palnd := nd.Find(txr.PalName, true)

		if gfxnd == nil || !gfxnd.Extracted || gfxnd.Cache == nil {
			return fmt.Errorf("gfx '%s' not cached", txr.GfxName)
		}
		if palnd == nil || !palnd.Extracted || palnd.Cache == nil {
			return fmt.Errorf("gfx '%s' not cached", txr.PalName)
		}

		names, err := txr.Extract(gfxnd.Cache.(*fgfx.Gfx), palnd.Cache.(*fgfx.Gfx), outName)
		if err != nil {
			return err
		}
		log.Printf("Texture '%s' extracted: %s", nd.Path, names)

		nd.ExtractedNames = names
		nd.Cache = txr
	}

	return nil
}
