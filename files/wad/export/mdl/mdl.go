package mdl

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"log"
	"path/filepath"

	"gowtools/config"
	"gowtools/files/wad"
	"gowtools/files/wad/export/mat"
	"gowtools/files/wad/export/mesh"
	"gowtools/utils"
)

type Model struct {
	TextureCount uint32
	MeshCount    uint32
	JointsCount  uint32
}

const ModelMagic = 0x0002000f
const FileSize = 0x48

func init() {
	wad.PregisterExporter(ModelMagic, &Model{})
}

func NewFromData(fmdl io.ReaderAt) (*Model, error) {
	var file [FileSize]byte
	if _, err := fmdl.ReadAt(file[:], 0); err != nil {
		return nil, err
	}

	mdl := new(Model)

	mdl.TextureCount = binary.LittleEndian.Uint32(file[0x14:0x18])
	mdl.MeshCount = binary.LittleEndian.Uint32(file[0x18:0x1c])
	mdl.JointsCount = binary.LittleEndian.Uint32(file[0x1c:0x20])

	return mdl, nil
}

func (mdl *Model) Extract(nd *wad.WadNode, bones []utils.Mat4, outName string) ([]string, error) {
	var textures []string
	var materials []*mat.Material
	for _, n := range nd.SubNodes {
		if n.Type == wad.NodeTypeLink {
			n = n.LinkTo
		}
		if n.Format == mat.MaterialMagic {
			if !n.Extracted || n.Cache == nil {
				return nil, errors.New("material not loaded before mesh")
			} else {
				mt := n.Cache.(*mat.Material)
				if mt == nil || mt.Layers == nil || len(mt.Layers) == 0 {
					return nil, fmt.Errorf("material '%s' not cached ", n.Path)
				}

				if mt.Layers[0].Texture != "" {
					t := nd.Find(mt.Layers[0].Texture, true)
					if !t.Extracted || t.ExtractedNames == nil || len(t.ExtractedNames) == 0 {
						return nil, errors.New("material not loaded before mesh")
					} else {
						name := t.ExtractedNames[0]
						textures = append(textures, filepath.Base(name))
						materials = append(materials, mt)
					}
				} else {
					log.Printf("Material without texture '%s'", n.Name)
					textures = append(textures, "")
					materials = append(materials, mt)
				}
			}
		}
	}

	for _, n := range nd.SubNodes {
		if n.Type == wad.NodeTypeLink {
			n = n.LinkTo
		}
		if n.Format == mesh.MeshMagic {
			if !n.Extracted || n.Cache == nil {
				return nil, errors.New("mesh not loaded before model")
			} else {
				m := n.Cache.(*mesh.Mesh)
				names, err := m.Extract(textures, materials, bones, outName)
				if err != nil {
					return nil, err
				}
				return names, nil
			}
		}
	}

	return nil, nil
}

func (*Model) ExtractFromNode(nd *wad.WadNode, outName string) error {
	log.Printf("Model '%s' extraction", nd.Path)
	reader, err := nd.DataReader()
	if err != nil {
		return err
	}

	mdl, err := NewFromData(reader)
	if err != nil {
		return err
	}

	if nd.Wad.Version == config.GoW2 {
		names, err := mdl.Extract(nd, nil, outName)
		if err != nil {
			return err
		}
		nd.ExtractedNames = names
		log.Printf("Model '%s' extracted: %s", nd.Path, names)
	}

	nd.Cache = mdl

	return nil
}
