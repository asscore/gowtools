package mat

import (
	"encoding/binary"
	"errors"
	"io"
	"log"
	"math"

	"gowtools/files/wad"
	"gowtools/utils"
)

type Layer struct {
	Texture string
	Flags   uint32
}

type Material struct {
	Layers []Layer
	Color  [4]float32

	Name string // zmienna pomocnicza wykorzystywana przy eksporcie obiektów
}

const MaterialMagic = 0x00000008
const HeaderSize = 0x38
const LayerSize = 0x40

func init() {
	wad.PregisterExporter(MaterialMagic, &Material{})
}

func NewFromData(fmat io.ReaderAt) (*Material, error) {
	buf := make([]byte, HeaderSize)
	if _, err := fmat.ReadAt(buf, 0); err != nil {
		return nil, err
	}

	magic := binary.LittleEndian.Uint32(buf[:4])
	if magic != MaterialMagic {
		return nil, errors.New("wrong magic")
	}

	mat := &Material{
		Layers: make([]Layer, binary.LittleEndian.Uint32(buf[0x34:0x38]))}

	mat.Color = [4]float32{
		math.Float32frombits(binary.LittleEndian.Uint32(buf[0x8:0xc])),
		math.Float32frombits(binary.LittleEndian.Uint32(buf[0xc:0x10])),
		math.Float32frombits(binary.LittleEndian.Uint32(buf[0x10:0x14])),
		//math.Float32frombits(binary.LittleEndian.Uint32(buf[0x14:0x18])),
		1.0,
	}

	for iTex := range mat.Layers {
		tbuf := make([]byte, LayerSize)

		if _, err := fmat.ReadAt(tbuf, int64(iTex*LayerSize+HeaderSize)); err != nil {
			return nil, err
		}

		flags := binary.LittleEndian.Uint32(tbuf[0x0:0x4])
		texture := utils.BytesToString(tbuf[0x10:0x28])

		mat.Layers[iTex] = Layer{Flags: flags, Texture: texture}
	}

	return mat, nil
}

//noinspection GoUnusedParameter
func (*Material) ExtractFromNode(nd *wad.WadNode, outName string) error {
	log.Printf("Material '%s' extraction", nd.Path)
	reader, err := nd.DataReader()
	if err != nil {
		return err
	}

	mat, err := NewFromData(reader)
	if err != nil {
		return err
	}

	mat.Name = nd.Path
	nd.Cache = mat

	return nil
}
