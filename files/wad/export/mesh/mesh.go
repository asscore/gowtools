package mesh

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	"gowtools/config"
	"gowtools/files/wad"
	"gowtools/files/wad/export/mat"
	"gowtools/utils"
)

type Packet struct {
	Uvs struct {
		U, V []float32
	}
	Trias struct {
		X, Y, Z []float32
		Skip    []bool
	}
	Norms struct {
		X, Y, Z []float32
	}
	Blend struct {
		R, G, B, A []uint16
	}
	Joints []uint16
	Offset uint32
}

type Object struct {
	Offset                uint32
	Type                  uint16
	DmaTagsCountPerPacket uint32
	MaterialId            uint8
	JointMapper           []uint32
	Packets               [][]Packet
}

type Group struct {
	Offset  uint32
	Objects []*Object
}

type Part struct {
	Offset uint32
	Groups []*Group
}

type Mesh struct {
	Parts []*Part
}

//noinspection GoNameStartsWithPackageName
const MeshMagic = 0x0001000f

func init() {
	wad.PregisterExporter(MeshMagic, &Mesh{})
}

func NewFromData(fmesh io.Reader, exlog *utils.Logger, version config.GoWVersion) (*Mesh, error) {
	file, err := ioutil.ReadAll(fmesh)
	if err != nil {
		return nil, err
	}

	u32 := func(idx uint32) uint32 {
		return binary.LittleEndian.Uint32(file[idx : idx+4])
	}
	u16 := func(idx uint32) uint16 {
		return binary.LittleEndian.Uint16(file[idx : idx+2])
	}
	u8 := func(idx uint32) uint8 {
		return file[idx]
	}

	if u32(0) != MeshMagic {
		return nil, fmt.Errorf("unknown mesh type")
	}

	var MeshHeaderSize uint32 = 0x50
	var ObjectHeaderSize uint32 = 0x20
	var GroupHeaderSize uint32 = 0xc

	partCount := u32(8)

	if version == config.GoW2 {
		MeshHeaderSize = 0x18
		GroupHeaderSize = 0x8
		partCount = uint32(u16(8))
	}

	parts := make([]*Part, partCount)
	for iPart := range parts {
		pPart := u32(MeshHeaderSize + uint32(iPart)*4)

		part := &Part{
			Offset: pPart,
			Groups: make([]*Group, u16(pPart+2)),
		}
		parts[iPart] = part

		exlog.Printf(" part: %d pos: %.6x groups: %d", iPart, part.Offset, len(part.Groups))
		for iGroup := range part.Groups {
			pGroup := pPart + u32(pPart+uint32(iGroup)*4+4)

			objectCount := u32(pGroup + 4)
			if version == config.GoW2 {
				objectCount = uint32(u16(pGroup + 4))
			}

			group := &Group{
				Offset:  pGroup,
				Objects: make([]*Object, objectCount),
			}
			part.Groups[iGroup] = group

			exlog.Printf("  group: %d pos: %.6x objects: %d", iGroup, group.Offset, len(group.Objects))
			for iObject := range group.Objects {
				pObject := pGroup + u32(GroupHeaderSize+pGroup+uint32(iObject)*4)

				objectType := u16(pObject)
				dmaCalls := u32(pObject+0xc) * uint32(u8(pObject+0x18))

				object := &Object{
					Offset:  pObject,
					Type:    objectType,
					Packets: make([][]Packet, dmaCalls),
				}
				group.Objects[iObject] = object

				object.DmaTagsCountPerPacket = u32(pObject + 4)
				object.MaterialId = u8(pObject + 8)

				exlog.Printf("   object: %d pos: %.6x type: %.2x material: %.2x", iObject, pObject,
					object.Type, object.MaterialId)
				for iDmaChain := uint32(0); iDmaChain < dmaCalls; iDmaChain++ {
					packetOffset := pObject + ObjectHeaderSize + iDmaChain*object.DmaTagsCountPerPacket*0x10
					exlog.Printf("    packets %d offset %.8x pps %.6x", iDmaChain, packetOffset,
						object.DmaTagsCountPerPacket)

					ds := NewMeshParserStream(file, object, packetOffset, exlog)
					if err := ds.ParsePackets(); err != nil {
						return nil, err
					}

					object.Packets[iDmaChain] = ds.Packets
				}

				if jmLen := u16(pObject + 0xa); jmLen != 0 {
					object.JointMapper = make([]uint32, jmLen)

					jointMapOffset := pObject + ObjectHeaderSize + dmaCalls*0x10*object.DmaTagsCountPerPacket
					for i := range object.JointMapper {
						object.JointMapper[i] = u32(jointMapOffset + uint32(i)*4)
					}
				}
			}
		}
	}

	mesh := &Mesh{Parts: parts}

	return mesh, nil
}

func (mesh *Mesh) Extract(textures []string, materials []*mat.Material, bones []utils.Mat4, outName string) ([]string, error) {
	oFileName := outName + ".obj"

	if err := os.MkdirAll(filepath.Dir(oFileName), 0777); err != nil {
		return nil, err
	}

	oFile, err := os.Create(oFileName)
	if err != nil {
		log.Fatalf("Cannot create file %s: %v", oFileName, err)
	}
	//noinspection GoUnhandledErrorResult
	defer oFile.Close()

	mFileName := outName + ".mtl"
	_, mRelativeName := filepath.Split(mFileName)
	mFile, err := os.Create(mFileName)
	if err != nil {
		return nil, err
	}

	//noinspection GoUnhandledErrorResult
	for i, tex := range textures {
		clr := materials[i].Color
		fmt.Fprintf(mFile, "newmtl %s\nKd %f %f %f\n", materials[i].Name, clr[0], clr[1], clr[2])
		if tex != "" {
			fmt.Fprintf(mFile, "map_Kd %s\n\n", tex)
		}
	}
	//noinspection GoUnhandledErrorResult
	mFile.Close()

	iV := 0
	iT := 0
	iN := 0
	var facesBuf bytes.Buffer

	enabledTransform := true
	if bones == nil { // druga część gry
		enabledTransform = false
	} else {
		// problem w ATHN01F.WAD z aoxpt - 'index out of range [86] with length 82'
		for _, part := range mesh.Parts {
			for _, group := range part.Groups {
				for _, object := range group.Objects {
					for i := range object.Packets {
						for _, packet := range object.Packets[i] {
							for i := range packet.Trias.X {
								if packet.Joints != nil && object.JointMapper != nil &&
									len(bones) < int(object.JointMapper[packet.Joints[i]]) {
									enabledTransform = false
									goto exit
								}
							}
						}
					}
				}
			}
		}
	exit:
	}
	if !enabledTransform {
		log.Println("Disabled transform coordinate")
	}

	w := func(format string, args ...interface{}) {
		//noinspection GoUnhandledErrorResult
		oFile.Write(([]byte)(fmt.Sprintf(format+"\n", args...)))
	}

	wi := func(format string, args ...interface{}) {
		facesBuf.WriteString(fmt.Sprintf(format+"\n", args...))
	}

	w("mtllib %s", mRelativeName)
	for iPart, part := range mesh.Parts {
		for iGroup, group := range part.Groups {
			wi("o p%.2dg%.2d", iPart, iGroup)
			for iObject, object := range group.Objects {
				if int(object.MaterialId) < len(materials) {
					wi("usemtl %s", materials[object.MaterialId].Name)
				}
				for i := range object.Packets {
					wi("g p%.2dg%.2do%.2dg.%2dp", iPart, iGroup, iObject, i)
					for _, packet := range object.Packets[i] {
						haveUV := packet.Uvs.U != nil
						haveNorm := packet.Norms.X != nil
						for i := range packet.Trias.X {
							vertex := utils.Vec3{packet.Trias.X[i], packet.Trias.Y[i], packet.Trias.Z[i]}
							if enabledTransform && bones != nil && packet.Joints != nil && object.JointMapper != nil {
								jointId := int(packet.Joints[i])
								bone := bones[object.JointMapper[jointId]]
								vertex = utils.TransformCoordinate(vertex, bone)
							}
							w("v %f %f %f", vertex[0], vertex[1], vertex[2])
							iV++
							if haveUV {
								w("vt %f %f", 3.0+packet.Uvs.U[i], 4.0-packet.Uvs.V[i])
								iT++
							}
							if haveNorm {
								w("vn %f %f %f", packet.Norms.X[i], packet.Norms.Y[i], packet.Norms.Z[i])
								iN++
							}
							if !packet.Trias.Skip[i] {
								if haveNorm {
									if haveUV {
										// vertex + texture + normal
										wi("f %d/%d/%d %d/%d/%d %d/%d/%d", iV-1, iT-1, iN-1, iV-2, iT-2, iN-2, iV, iT, iN)
									} else {
										// vertex + normal
										wi("f %d//%d %d//%d %d//%d", iV-1, iN-1, iV-2, iN-2, iV, iN)
									}
								} else {
									if haveUV {
										// vertex + texture
										wi("f %d/%d %d/%d %d/%d", iV-1, iT-1, iV-2, iT-2, iV, iT)
									} else {
										// vertex
										wi("f %d %d %d", iV-1, iV-2, iV)
									}
								}
							}
						}
					}
				}
			}
		}
	}

	//noinspection GoUnhandledErrorResult
	oFile.Write(facesBuf.Bytes())

	return []string{mFileName, oFileName}, nil
}

//noinspection GoUnusedParameter
func (*Mesh) ExtractFromNode(nd *wad.WadNode, outName string) error {
	log.Printf("Mesh '%s' extraction", nd.Name)
	reader, err := nd.DataReader()
	if err != nil {
		return err
	}

	fpath := filepath.Join("logs", nd.Wad.Name, fmt.Sprintf("%s.mesh.log", nd.Name))
	_ = os.MkdirAll(filepath.Dir(fpath), 0777)
	f, _ := os.Create(fpath)
	//noinspection GoUnhandledErrorResult
	defer f.Close()
	logger := utils.Logger{Writer: f}

	mesh, err := NewFromData(reader, &logger, nd.Wad.Version)
	if err != nil {
		return err
	}

	nd.Cache = mesh

	return nil
}
