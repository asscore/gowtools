package vif

import "fmt"

//noinspection GoNameStartsWithPackageName
type VifCode uint32

//noinspection GoNameStartsWithPackageName, GoUnusedConst
const (
	VifCmdNop      = 0x00 // No Operation
	VifCmdStcycl   = 0x01 // Sets CYCLE register
	VifCmdOffset   = 0x02 // Sets OFFSET register (VIF1)
	VifCmdBase     = 0x03 // Sets BASE register (VIF1)
	VifCmdItop     = 0x04 // Sets ITOPS register
	VifCmdStmod    = 0x05 // Sets MODE register
	VifCmdMskpath3 = 0x06 // Mask GIF transfer (VIF1)
	VifCmdMark     = 0x07 // Sets Mark register
	VifCmdFlushe   = 0x10 // Wait for end of microprogram
	VifCmdFlush    = 0x11 // Wait for end of microprogram & Path 1/2 GIF xfer (VIF1)
	VifCmdFlusha   = 0x13 // Wait for end of microprogram & all Path GIF xfer (VIF1)
	VifCmdMscal    = 0x14 // Activate microprogram
	VifCmdMscnt    = 0x17 // Execute microrprogram continuously
	VifCmdMscalf   = 0x15 // Activate microprogram (VIF1)
	VifCmdStmask   = 0x20 // Sets MASK register
	VifCmdStrow    = 0x30 // Sets ROW register
	VifCmdStcol    = 0x31 // Sets COL register
	VifCmdMpg      = 0x4A // Load microprogram
	VifCmdDirect   = 0x50 // Transfer data to GIF (VIF1)
	VifCmdDirecthl = 0x51 // Transfer data to GIF but stall for Path 3 IMAGE mode (VIF1)
)

func (v VifCode) Cmd() uint8 {
	return uint8((v >> 24) & 0xff)
}

func (v VifCode) Num() uint8 {
	return uint8((v >> 16) & 0xff)
}

func (v VifCode) Imm() uint16 {
	return uint16(v & 0xffff)
}

func (v VifCode) IsIrq() bool {
	return (v>>31)&1 != 0
}

func (v VifCode) String() string {
	return fmt.Sprintf("VifCode{Cmd:0x%.2x; Num:0x%.2x; Imm:0x%.4x; Irq:%t}",
		v.Cmd(), v.Num(), v.Imm(), v.IsIrq())
}

func NewCode(raw uint32) VifCode {
	return VifCode(raw)
}
