package mesh

import (
	"encoding/binary"
	"fmt"

	"gowtools/files/wad/export/mesh/dma"
	"gowtools/files/wad/export/mesh/vif"
	"gowtools/utils"
)

var unpackBuffersBases = []uint32{0, 0x155, 0x2ab}

const GSFixedPoint8 = 16.0
const GSFixedPoint24 = 4096.0

//noinspection GoNameStartsWithPackageName
type MeshParserStream struct {
	Data                []byte
	Offset              uint32
	Packets             []Packet
	Object              *Object
	Log                 *utils.Logger
	State               *MeshParserState
	LastPacketDataStart uint32
	LastPacketDataEnd   uint32
}

//noinspection GoNameStartsWithPackageName
type MeshParserState struct {
	Xyzw       []byte
	Rgba       []byte
	Uv         []byte
	UvWidth    int
	Norm       []byte
	Boundaries []byte
	VertexMeta []byte
	Buffer     int
}

func NewMeshParserStream(data []byte, object *Object, offset uint32, exlog *utils.Logger) *MeshParserStream {
	return &MeshParserStream{
		Data:    data,
		Object:  object,
		Offset:  offset,
		Log:     exlog,
		Packets: make([]Packet, 0),
	}
}

func (ms *MeshParserStream) flushState() error {
	if ms.State != nil {
		packet, err := ms.State.ToPacket(ms.Log, ms.LastPacketDataStart)
		if err != nil {
			return err
		}
		if packet != nil {
			ms.Packets = append(ms.Packets, *packet)
		}
	}
	ms.State = &MeshParserState{}
	return nil
}

func (ms *MeshParserStream) ParsePackets() error {
	for i := uint32(0); i < ms.Object.DmaTagsCountPerPacket; i++ {
		dmaPackPos := ms.Offset + i*0x10
		dmaPack := dma.NewTag(binary.LittleEndian.Uint64(ms.Data[dmaPackPos:]))

		ms.Log.Printf("     dma offset: %.8x packet: %d pos: %.6x rows: %.4x end: %.6x", dmaPackPos,
			i, dmaPack.Addr()+ms.Object.Offset, dmaPack.Qwc(), dmaPack.Addr()+ms.Object.Offset+dmaPack.Qwc()*16)
		switch dmaPack.Id() {
		case dma.DmaTagRef:
			ms.LastPacketDataStart = dmaPack.Addr() + ms.Object.Offset
			ms.LastPacketDataEnd = ms.LastPacketDataStart + dmaPack.Qwc()*0x10
			ms.Log.Printf("      vif pack start: %.6x + %.6x = %.6x => %.6x", ms.Offset, dmaPack.Addr(),
				ms.LastPacketDataStart, ms.LastPacketDataEnd)
			if err := ms.ParseVif(); err != nil {
				return fmt.Errorf("error when parsing vif stream triggered by dma_tag_ref: %v", err)
			}
		case dma.DmaTagRet:
			if dmaPack.Qwc() != 0 {
				return fmt.Errorf("not support dma_tag_ret with qwc != 0 (%d)", dmaPack.Qwc())
			}
			if i != ms.Object.DmaTagsCountPerPacket-1 {
				return fmt.Errorf("dma_tag_ret not in end of stream (%d != %d)", i, ms.Object.DmaTagsCountPerPacket-1)
			} else {
				ms.Log.Printf("     dma_tag_ret at %.8x", dmaPackPos)
			}
		default:
			return fmt.Errorf("unknown dma packet %v in mesh stream at %.8x i = %.2x < %.2x", dmaPack,
				dmaPackPos, i, ms.Object.DmaTagsCountPerPacket)
		}
	}
	//noinspection GoUnhandledErrorResult
	ms.flushState()

	return nil
}

func (state *MeshParserState) ToPacket(exlog *utils.Logger, pos uint32) (*Packet, error) {
	if state.Xyzw == nil {
		if state.Uv != nil || state.Norm != nil || state.VertexMeta != nil || state.Rgba != nil {
			return nil, fmt.Errorf("empty xyzw array, possibly incorrect data: %x, state: %+#v", pos, state)
		}
		return nil, nil
	}

	packet := &Packet{}
	packet.Offset = pos

	triasCount := len(state.Xyzw) / 8
	packet.Trias.X = make([]float32, triasCount)
	packet.Trias.Y = make([]float32, triasCount)
	packet.Trias.Z = make([]float32, triasCount)
	packet.Trias.Skip = make([]bool, triasCount)
	for i := range packet.Trias.X {
		bp := i * 8
		packet.Trias.X[i] = float32(int16(binary.LittleEndian.Uint16(state.Xyzw[bp:bp+2]))) / GSFixedPoint8
		packet.Trias.Y[i] = float32(int16(binary.LittleEndian.Uint16(state.Xyzw[bp+2:bp+4]))) / GSFixedPoint8
		packet.Trias.Z[i] = float32(int16(binary.LittleEndian.Uint16(state.Xyzw[bp+4:bp+6]))) / GSFixedPoint8
		packet.Trias.Skip[i] = state.Xyzw[bp+7]&0x80 != 0
	}

	if state.Uv != nil {
		switch state.UvWidth {
		case 2:
			uvCount := len(state.Uv) / 4
			packet.Uvs.U = make([]float32, uvCount)
			packet.Uvs.V = make([]float32, uvCount)
			for i := range packet.Uvs.U {
				bp := i * 4
				packet.Uvs.U[i] = float32(int16(binary.LittleEndian.Uint16(state.Uv[bp:bp+2]))) / GSFixedPoint24
				packet.Uvs.V[i] = float32(int16(binary.LittleEndian.Uint16(state.Uv[bp+2:bp+4]))) / GSFixedPoint24
			}
		case 4:
			uvCount := len(state.Uv) / 8
			packet.Uvs.U = make([]float32, uvCount)
			packet.Uvs.V = make([]float32, uvCount)
			for i := range packet.Uvs.U {
				bp := i * 8
				packet.Uvs.U[i] = float32(int32(binary.LittleEndian.Uint32(state.Uv[bp:bp+4]))) / GSFixedPoint24
				packet.Uvs.V[i] = float32(int32(binary.LittleEndian.Uint32(state.Uv[bp+4:bp+8]))) / GSFixedPoint24
			}
		}
	}

	if state.Norm != nil {
		normCount := len(state.Norm) / 3
		packet.Norms.X = make([]float32, normCount)
		packet.Norms.Y = make([]float32, normCount)
		packet.Norms.Z = make([]float32, normCount)
		for i := range packet.Norms.X {
			bp := i * 3
			packet.Norms.X[i] = float32(int8(state.Norm[bp])) / 100.0
			packet.Norms.Y[i] = float32(int8(state.Norm[bp+1])) / 100.0
			packet.Norms.Z[i] = float32(int8(state.Norm[bp+2])) / 100.0
		}
	}

	if state.Rgba != nil {
		rgbaCount := len(state.Rgba) / 4
		packet.Blend.R = make([]uint16, rgbaCount)
		packet.Blend.G = make([]uint16, rgbaCount)
		packet.Blend.B = make([]uint16, rgbaCount)
		packet.Blend.A = make([]uint16, rgbaCount)
		for i := range packet.Blend.R {
			bp := i * 4
			packet.Blend.R[i] = uint16(state.Rgba[bp])
			packet.Blend.G[i] = uint16(state.Rgba[bp+1])
			packet.Blend.B[i] = uint16(state.Rgba[bp+2])
			packet.Blend.A[i] = uint16(state.Rgba[bp+3])
		}
	}

	if state.VertexMeta != nil {
		blocks := len(state.VertexMeta) / 0x10
		vertexes := len(packet.Trias.X)

		packet.Joints = make([]uint16, vertexes)

		vertNum := 0
		for i := 0; i < blocks; i++ {
			block := state.VertexMeta[i*16 : i*16+16]

			blockVerts := int(block[0])

			for j := 0; j < blockVerts; j++ {
				packet.Joints[vertNum+j] = uint16(block[13] >> 4)
			}

			vertNum += blockVerts

			if block[1] != 0 {
				if i != blocks-1 {
					return nil, fmt.Errorf("block count != blocks: %v <= %v", blocks, i)
				}
			}
		}
		if vertNum != vertexes {
			return nil, fmt.Errorf("vertnum != vertexes count: %v <= %v", vertNum, vertexes)
		}
	}

	exlog.Printf("       = Flush xyzw:%t, rgba:%t, uv:%t, norm:%t, vmeta:%t (%d)",
		state.Xyzw != nil, state.Rgba != nil, state.Uv != nil,
		state.Norm != nil, state.VertexMeta != nil, len(packet.Trias.X))

	return packet, nil
}

func (ms *MeshParserStream) ParseVif() error {
	data := ms.Data[ms.LastPacketDataStart:ms.LastPacketDataEnd]
	pos := uint32(0)
	for {
		pos = ((pos + 3) / 4) * 4
		if pos >= uint32(len(data)) {
			break
		}
		tagPos := pos
		rawVifCode := binary.LittleEndian.Uint32(data[pos:])
		vifCode := vif.NewCode(rawVifCode)

		pos += 4
		if vifCode.Cmd() > 0x60 {
			vifComponents := ((vifCode.Cmd() >> 2) & 0x3) + 1
			vifWidth := []uint32{32, 16, 8, 4}[vifCode.Cmd()&0x3]

			vifBlockSize := uint32(vifComponents) * ((vifWidth * uint32(vifCode.Num())) / 8)

			vifIsSigned := (vifCode.Imm()>>14)&1 == 0
			vifUseTops := (vifCode.Imm()>>15)&1 != 0
			vifTarget := uint32(vifCode.Imm() & 0x3ff)

			vifBufferBase := 1
			for _, base := range unpackBuffersBases {
				if vifTarget >= base {
					vifBufferBase++
				} else {
					break
				}
			}
			if ms.State == nil {
				ms.State = &MeshParserState{Buffer: vifBufferBase}
			} else if vifBufferBase != ms.State.Buffer {
				if err := ms.flushState(); err != nil {
					return err
				}
				ms.State.Buffer = vifBufferBase
			}
			handledBy := ""

			vifBlock := data[pos : pos+vifBlockSize]

			errorAlreadyPresent := func(handler string) error {
				return fmt.Errorf("%s already present: %.6x", handler, tagPos+ms.LastPacketDataStart)
			}

			var detectingErr error = nil

			switch vifWidth {
			case 32:
				if vifIsSigned {
					switch vifComponents {
					case 4:
						switch vifTarget {
						case 0x000, 0x155, 0x2ab:
							if ms.State.VertexMeta != nil {
								detectingErr = errorAlreadyPresent("vertex meta")
							}
							ms.State.VertexMeta = vifBlock
							handledBy = "vmta"
						default:
							if ms.State.Boundaries != nil {
								detectingErr = errorAlreadyPresent("boundaries")
							}
							ms.State.Boundaries = vifBlock
							handledBy = "bndr"
						}
					case 2:
						handledBy = " uv4"
						if ms.State.Uv == nil {
							ms.State.Uv = vifBlock
							ms.State.UvWidth = 4
						} else {
							detectingErr = errorAlreadyPresent("uv")
						}
					}
				}
			case 16:
				if vifIsSigned {
					switch vifComponents {
					case 4:
						if ms.State.Xyzw == nil {
							ms.State.Xyzw = vifBlock
							handledBy = "xyzw"
						} else {
							detectingErr = errorAlreadyPresent("xyzw")
						}
					case 2:
						if ms.State.Uv == nil {
							ms.State.Uv = vifBlock
							handledBy = " uv2"
							ms.State.UvWidth = 2
						} else {
							detectingErr = errorAlreadyPresent("uv")
						}
					}
				}
			case 8:
				switch vifComponents {
				case 3:
					if vifIsSigned {
						if ms.State.Norm == nil {
							ms.State.Norm = vifBlock
							handledBy = "norm"
						} else {
							detectingErr = errorAlreadyPresent("norm")
						}
					}
				case 4:
					ms.State.Rgba = vifBlock
					if ms.State.Rgba == nil {
						handledBy = "rgba"
					} else {
						handledBy = "rgbA"
					}
				}
			}

			ms.Log.Printf("      %.6x vif unpack [%s]: %.2x elements: %.2x components: %d type: %.2d target: %.3x sign: %t tops: %t size: %.6x",
				ms.LastPacketDataStart+tagPos, handledBy, vifCode.Cmd(), vifCode.Num(), vifComponents, vifWidth, vifTarget, vifIsSigned, vifUseTops, vifBlockSize)

			if detectingErr != nil || handledBy == "" {
				ms.Log.Printf("%v", detectingErr)
				if detectingErr != nil {
					return detectingErr
				} else {
					return fmt.Errorf("block 0x%.6x (cmd 0x%.2x; %d bit; %d components; %d elements; sign %t; tops %t; target: %.3x; size: %.6x) not handled",
						tagPos+ms.LastPacketDataStart, vifCode.Cmd(), vifWidth, vifComponents, vifCode.Num(), vifIsSigned, vifUseTops, vifTarget, vifBlockSize)
				}
			}
			pos += vifBlockSize
		} else {
			switch vifCode.Cmd() {
			case vif.VifCmdNop:
				ms.Log.Printf("      %.6x Nop", ms.LastPacketDataStart+tagPos)
			case vif.VifCmdStcycl:
			case vif.VifCmdMscal:
				if err := ms.flushState(); err != nil {
					return err
				}
			case vif.VifCmdStrow:
				ms.Log.Printf("      %.6x Strow proc command", ms.LastPacketDataStart+tagPos)
				pos += 0x10
			default:
				ms.Log.Printf("      %.6x unknown vif command: %.2x:%.2x", ms.LastPacketDataStart+tagPos,
					vifCode.Cmd(), vifCode.Num())
			}
		}
	}

	return nil
}
