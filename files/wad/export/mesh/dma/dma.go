package dma

import (
	"fmt"
)

//noinspection GoNameStartsWithPackageName
type DmaTag uint64

//noinspection GoNameStartsWithPackageName, GoUnusedConst
const (
	DmaTagCnts = 0x00 // T=QWC D=QWC+1 MADR => STADR
	DmaTagRefe = 0x00 // T=ADDR then END
	DmaTagCnt  = 0x01 // T=QWC D=QWC+1
	DmaTagNext = 0x02 // T=QWC D=ADDR
	DmaTagRef  = 0x03 // D=D+1 T=ADDR
	DmaTagRefs = 0x04 //.. + stall ctrl
	DmaTagCall = 0x05 // T=QWC D=ADDR QWC+1 => ASR0
	DmaTagRet  = 0x06 // T=QWC (ASR0 => D) if !ASR0 then END
	DmaTagEnd  = 0x07 // T=QWC then END
)

var dmaTagIdToString = []string{
	"cnts/refe", "cnt", "next", "ref",
	"refs", "call", "ret", "end",
}

func (p DmaTag) Qwc() uint32 {
	return uint32(p & 0xffff)
}

func (p DmaTag) Id() uint8 {
	return uint8((p >> 28) & 0x7)
}

func (p DmaTag) Addr() uint32 {
	return uint32((p >> 32) & 0x7FFFFFFF)
}

func (p DmaTag) IsSpr() bool {
	return (p>>63)&1 != 0
}

func (p DmaTag) IsIrq() bool {
	return (p>>31)&1 != 0
}

func (p DmaTag) String() string {
	return fmt.Sprintf("DmaTag{Id:%-9s; Addr:0x%.4x; Qwc:0x%.2x; Spr:%t; Irq:%t}",
		dmaTagIdToString[p.Id()], p.Addr(), p.Qwc(), p.IsSpr(), p.IsIrq())
}

func NewTag(raw uint64) DmaTag {
	return DmaTag(raw)
}
