package sbk

import (
	"bytes"
	"encoding/binary"
	"io"
	"log"
	"os"
	"path/filepath"

	"gowtools/files/vag"
	"gowtools/files/wad"
	"gowtools/utils"
)

//noinspection GoNameStartsWithPackageName
const SbkVagMagic = 0x00040018

type Sound struct {
	Name     string
	StreamId uint32
}

type Sbk struct {
	Sounds []Sound
}

func init() {
	wad.PregisterExporter(SbkVagMagic, &Sbk{})
}

func NewFromData(f io.ReaderAt) (*Sbk, error) {
	var soundsCount uint32
	if err := binary.Read(io.NewSectionReader(f, 4, 8), binary.LittleEndian, &soundsCount); err != nil {
		return nil, err
	}

	sbk := &Sbk{
		Sounds: make([]Sound, soundsCount),
	}

	for i := range sbk.Sounds {
		var buf [28]byte
		_, err := f.ReadAt(buf[:], 8+int64(i)*28)
		if err != nil {
			return nil, err
		}

		sbk.Sounds[i].Name = utils.BytesToString(buf[:24])
		sbk.Sounds[i].StreamId = binary.LittleEndian.Uint32(buf[24:])
	}

	return sbk, nil
}

func (sbk *Sbk) Extract(nd *wad.WadNode, outName string) ([]string, error) {
	names := make([]string, 0)

	data, err := nd.DataRead()
	if err != nil {
		return nil, err
	}

	if err := os.MkdirAll(outName, 0777); err != nil {
		return nil, err
	}

	for iSnd, snd := range sbk.Sounds {
		end := nd.Size
		if iSnd != len(sbk.Sounds)-1 {
			end = sbk.Sounds[iSnd+1].StreamId
		}

		vReader := bytes.NewReader(data[snd.StreamId:end])
		v, err := vag.NewVagFromReader(vReader)
		if err != nil {
			return nil, err
		}
		wFile, err := os.Create(filepath.Join(outName, snd.Name+".wav"))
		if err != nil {
			return nil, err
		}
		//noinspection GoDeferInLoop, GoUnhandledErrorResult
		defer wFile.Close()

		if _, err := v.AsWave(wFile); err != nil {
			return nil, err
		}

		names = append(names, snd.Name)
	}

	return names, nil
}

func (*Sbk) ExtractFromNode(nd *wad.WadNode, outName string) error {
	log.Printf("Sbk '%s' extraction", nd.Path)
	reader, err := nd.DataReader()
	if err != nil {
		return err
	}

	sbk, err := NewFromData(reader)
	if err != nil {
		return err
	}

	names, err := sbk.Extract(nd, outName)
	if err != nil {
		return err
	}
	nd.ExtractedNames = names
	log.Printf("Sbk '%s' extracted: %s", nd.Path, names)

	nd.Cache = sbk

	return nil
}
