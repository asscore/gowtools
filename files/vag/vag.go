package vag

import (
	"bytes"
	"encoding/binary"
	"errors"
	"io"
	"os"
	"path/filepath"

	"gowtools/libs/adpcm"
	"gowtools/utils"
)

// VAGp - Sony SDK format
type Vag struct {
	Channels   uint32
	DataSize   []byte
	SampleRate uint32
}

func NewVagFromReader(r io.Reader) (*Vag, error) {
	var header [0x30]byte
	if _, err := r.Read(header[:]); err != nil {
		return nil, err
	}

	if bytes.Compare([]byte{0x56, 0x41, 0x47, 0x70}, header[:0x4]) != 0 { // "WAGp"
		return nil, errors.New("magic not matched")
	}

	vag := &Vag{
		Channels:   1, // w tej wersji
		DataSize:   make([]byte, binary.BigEndian.Uint32(header[0xc:0x10])),
		SampleRate: binary.BigEndian.Uint32(header[0x10:0x14]),
	}

	// Version - header[0x4:0x8] = 00000020 (BigEndian)

	if _, err := r.Read(vag.DataSize); err != nil {
		return nil, err
	}

	return vag, nil
}

func (vag *Vag) AsWave(w io.Writer) (int, error) {
	if vag.Channels > 1 {
		panic("Not mono not supported")
	}

	if err := utils.WaveWriteHeader(w, uint16(vag.Channels), vag.SampleRate, uint32(adpcm.AdpcmSizeToWaveSize(len(vag.DataSize)))); err != nil {
		return 0, err
	}

	adpcmStream := adpcm.NewAdpcmToWaveStream(w)
	n, err := adpcmStream.Write(vag.DataSize)
	if err != nil {
		return 0, err
	}

	return n, err
}

func WaveWrite(inFolder string, name string) (*Vag, error) {
	vagFile, err := os.Open(filepath.Join(inFolder, name))
	if err != nil {
		return nil, err
	}
	//noinspection GoUnhandledErrorResult
	defer vagFile.Close()

	vag, err := NewVagFromReader(vagFile)
	if err != nil {
		return nil, err
	}

	wavFile, err := os.Create(filepath.Join(inFolder, name+".wav"))
	if err != nil {
		return nil, err
	}
	//noinspection GoUnhandledErrorResult
	defer wavFile.Close()

	if _, err := vag.AsWave(wavFile); err != nil {
		return nil, err
	}

	return vag, nil
}
