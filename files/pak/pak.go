package pak

import (
	"errors"
	"io"
	"log"
	"math"
	"os"
	"path/filepath"
	"strconv"

	"gowtools/files/toc"
	"gowtools/utils"
)

func GetPartName(gameFolder string, part uint32) string {
	return filepath.Join(gameFolder, "PART"+strconv.Itoa(int(part+1))+".PAK")
}

func Unpack(gameFolder string, outFolder string, entries []*toc.RawTocEntry) error {
	partPresents := make(map[uint32]bool, 0)
	for _, e := range entries {
		if _, ok := partPresents[e.Part]; !ok {
			if _, err := os.Stat(GetPartName(gameFolder, e.Part)); !os.IsNotExist(err) {
				partPresents[e.Part] = true
			} else {
				partPresents[e.Part] = false
			}
		}
	}

	var currPart uint32 = math.MaxUint32
	var fp *os.File

	defer func() {
		if currPart != math.MaxUint32 {
			//noinspection GoUnhandledErrorResult
			fp.Close()
		}
	}()

	if err := os.MkdirAll(outFolder, 0777); err != nil {
		return err
	}

	for i, e := range entries {
		if partPresents[e.Part] {
			if e.Part != currPart {
				newPart, err := os.Open(GetPartName(gameFolder, e.Part))
				if err != nil {
					return err
				}
				//noinspection GoUnhandledErrorResult
				fp.Close()
				currPart = e.Part
				fp = newPart
			}

			fo, err := os.Create(filepath.Join(outFolder, e.Name))
			if err != nil {
				return err
			}
			//noinspection GoDeferInLoop, GoUnhandledErrorResult
			defer fo.Close()

			if _, err := fp.Seek(int64(e.Sector*utils.SectorSize), io.SeekStart); err != nil {
				log.Fatalf("Error when seeking: %v", err)
			}

			log.Printf("[%.4d/%.4d] Unpacking (p:%v b:0x%.8x s:%v) %s\n",
				i+1, len(entries), e.Part+1, e.Sector*utils.SectorSize, e.Size, e.Name)

			written, err := io.CopyN(fo, fp, int64(e.Size))
			if err != nil && err != io.EOF {
				return err
			} else if written != int64(e.Size) {
				return errors.New("file not copied fully")
			}
			//noinspection GoUnhandledErrorResult
			fo.Close()
		}
	}

	return nil
}
