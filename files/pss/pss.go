package pss

import (
	"bytes"
	"encoding/binary"
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"

	"gowtools/libs/adpcm"
	"gowtools/utils"
)

type StreamInfo struct {
	SampleRate uint32
	Channels   uint32
	Interleave uint32
	DataSize   uint32
	Count      uint32
}

func Demux(inFolder string, name string) (*StreamInfo, error) {
	data, err := ioutil.ReadFile(filepath.Join(inFolder, name))
	if err != nil {
		return nil, err
	}

	video := make([]byte, 0)
	audio := make([][]byte, 6)

	idx := 0
	for {
		header := data[idx : idx+4]
		idx += 4

		if bytes.Compare([]byte{0x00, 0x00, 0x01, 0xba}, header) == 0 { // MPEG start
			idx += 0x0a
		} else if bytes.Compare([]byte{0x00, 0x00, 0x01, 0xb9}, header) == 0 { // MPEG end
			break
		} else if bytes.Compare([]byte{0x00, 0x00, 0x01, 0xbb}, header) == 0 { // !?
			idx += int(binary.BigEndian.Uint16(data[idx:idx+2]) + 2)
		} else if bytes.Compare([]byte{0x00, 0x00, 0x01, 0xbe}, header) == 0 { // audio?
			idx += int(binary.BigEndian.Uint16(data[idx:idx+2]) + 2)
		} else if bytes.Compare([]byte{0x00, 0x00, 0x01}, header[:3]) == 0 {
			if header[3] >= 0xbd && header[3] <= 0xdf && header[3] != 0xbe {
				//streamType := (data[idx+0x10] + data[idx+0x12]) & 0xf0
				streamId := (data[idx+0x10] + data[idx+0x12]) & 0x0f

				dataLen := int(binary.BigEndian.Uint16(data[idx : idx+2]))
				headerOffset := int(data[idx+4 : idx+5][0])
				dataLen -= headerOffset + 7
				idx += headerOffset + 9
				if streamId > 0 {
					streamId--
				}
				audio[streamId] = append(audio[streamId], data[idx:idx+dataLen]...)
				idx += dataLen
			} else if header[3] >= 0xe0 && header[3] <= 0xef {
				dataLen := int(binary.BigEndian.Uint16(data[idx : idx+2]))
				headerOffset := int(data[idx+4 : idx+5][0])
				dataLen -= headerOffset + 3
				idx += headerOffset + 5
				video = append(video, data[idx:idx+dataLen]...)
				idx += dataLen
			} else {
				return nil, errors.New("unexpected block")
			}
		} else {
			return nil, errors.New("unexpected block")
		}
	}

	fv, err := os.Create(filepath.Join(inFolder, name+".m2v"))
	if err != nil {
		return nil, err
	}
	//noinspection GoUnhandledErrorResult
	defer fv.Close()

	if _, err := fv.Write(video); err != nil {
		return nil, err
	}

	var si = &StreamInfo{}

	for i := range audio {
		if len(audio[i]) > 0 {
			if bytes.Compare([]byte{0x53, 0x53, 0x68, 0x64}, audio[i][:0x4]) != 0 { // "SShd"
				return nil, errors.New("magic not matched")
			}

			si.SampleRate = binary.LittleEndian.Uint32(audio[i][0xc:0x10])
			si.Channels = binary.LittleEndian.Uint32(audio[i][0x10:0x14])
			si.Interleave = binary.LittleEndian.Uint32(audio[i][0x14:0x18])
			si.DataSize = binary.LittleEndian.Uint32(audio[i][0x24:0x28])
			si.Count++

			if si.Interleave != 0x200 && si.Interleave != 0x20 {
				return nil, errors.New("unsupported audio")
			}

			var ext string
			if i == 0 {
				ext = ".wav"
			} else {
				ext = "." + strconv.Itoa(i) + ".wav"
			}

			fa, err := os.Create(filepath.Join(inFolder, name+ext))
			if err != nil {
				return nil, err
			}
			//noinspection GoDeferInLoop, GoUnhandledErrorResult
			defer fa.Close()

			if si.Interleave == 0x200 { // PCM
				if err := utils.WaveWriteHeader(fa, uint16(si.Channels), si.SampleRate, si.DataSize); err != nil {
					return nil, err
				}

				output := make([]byte, 0)

				idx1 := 0
				idx2 := 0x200
				for k := 0; k < len(audio[i][40:])/2; k += 2 {
					if k > 0 && k%0x200 == 0 {
						idx1 += 0x200
						idx2 += 0x200
					}

					output = append(output, audio[i][40+k+idx1:40+k+idx1+2]...)
					output = append(output, audio[i][40+k+idx2:40+k+idx2+2]...)
				}

				if _, err := fa.Write(output); err != nil {
					return nil, err
				}
			} else if si.Interleave == 0x20 { // ADPCM
				var inBlockSize = 0x20 * si.Channels

				var in = make([]byte, inBlockSize)
				var out = make([]byte, adpcm.AdpcmSizeToWaveSize(int(inBlockSize)))

				var adpcmStream = make([]*adpcm.AdpcmStream, si.Channels)
				for i := range adpcmStream {
					adpcmStream[i] = adpcm.NewAdpcmStream()
				}

				if err := utils.WaveWriteHeader(fa, uint16(si.Channels), si.SampleRate, uint32(adpcm.AdpcmSizeToWaveSize(int(si.DataSize)))); err != nil {
					return nil, err
				}

				r := bytes.NewReader(audio[i][40:])

				inLeft := int(si.DataSize / si.Channels)
				n := 0
				for inLeft > 0 {
					if _, err := r.Read(in); err != nil {
						return nil, err
					}

					dataLen := 0x20
					if inLeft < 0x20 {
						dataLen = inLeft
					}

					for c, s := range adpcmStream {
						buf, err := s.Unpack(in[c*0x20 : c*0x20+dataLen])
						if err != nil {
							return nil, err
						}

						for k := 0; k < len(buf)/2; k++ {
							j := uint32(k * 2) // 16bit
							outPos := j*si.Channels + uint32(c*2)
							out[outPos] = buf[j]
							out[outPos+1] = buf[j+1]
						}
					}

					if wn, err := fa.Write(out); err != nil {
						return si, err
					} else {
						n += wn
					}

					inLeft -= 0x20
				}
			}
		}
	}

	if si.Count == 0 {
		return nil, errors.New("no audio stream")
	}

	return si, nil
}
