package psarc

import (
	"bytes"
	"compress/zlib"
	"fmt"
	"io"
)

type File struct {
	Psarc  *Psarc
	Entry  Entry
	Buffer *bytes.Buffer
}

func (f *File) InitBuffer() error {
	buf := &bytes.Buffer{}

	if f.Entry.OriginalSize == 0 {
		return nil
	}

	blockIndex := f.Entry.BlockListStart
	compressedFileOffset := int64(0)
	outFileOffset := int64(0)
	compressedBuf := make([]byte, f.Psarc.Header.BlockSize)

	for outFileOffset < f.Entry.OriginalSize {
		compressedArchiveOffset := f.Entry.StartOffset + compressedFileOffset
		compressedBlockSize := f.Psarc.BlockSizes[blockIndex]

		if compressedBlockSize == 0 {
			if _, err := f.Psarc.File.ReadAt(compressedBuf, compressedArchiveOffset); err != nil {
				return fmt.Errorf("[psarc.File.InitBuffer blockSize=0 ReadAt] %v", err)
			}

			if _, err := buf.Write(compressedBuf); err != nil {
				return err
			}

			compressedFileOffset += int64(f.Psarc.Header.BlockSize)
			outFileOffset += int64(f.Psarc.Header.BlockSize)
		} else {
			compressedBlock := compressedBuf[:compressedBlockSize]
			if _, err := f.Psarc.File.ReadAt(compressedBlock, compressedArchiveOffset); err != nil {
				return fmt.Errorf("[psarc.File.InitBuffer blockSize!=0 ReadAt] %v", err)
			}

			if compressedBlock[0] == 0x78 && compressedBlock[1] == 0xda {
				if zr, err := zlib.NewReader(bytes.NewReader(compressedBlock)); err != nil {
					return err
				} else {
					if w, err := io.Copy(buf, zr); err != nil {
						return err
					} else {
						outFileOffset += w
					}
				}
			} else {
				if _, err := buf.Write(compressedBlock); err != nil {
					return err
				}

				outFileOffset += int64(compressedBlockSize)
			}

			compressedFileOffset += int64(compressedBlockSize)
		}

		blockIndex++
	}

	f.Buffer = buf

	return nil
}
