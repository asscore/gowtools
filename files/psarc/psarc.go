package psarc

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"

	"gowtools/utils"
)

type Psarc struct {
	File       *os.File
	Header     Header
	Reader     *io.SectionReader
	BlockSizes []uint32
	Entries    []Entry
}

func (p *Psarc) ParseHeader() error {
	var b [RawHeaderSize]byte
	if _, err := p.File.ReadAt(b[:], 0); err != nil {
		return fmt.Errorf("[psarc] Header ReadAt(..): %v", err)
	}
	p.Header.FromBuffer(b[:])

	return nil
}

func (p *Psarc) ParseEntries() error {
	b := make([]byte, p.Header.NumFiles*RawEntrySize)
	if _, err := p.File.ReadAt(b, RawHeaderSize); err != nil {
		return fmt.Errorf("[psarc] Entries ReadAt(..): %v", err)
	}

	p.Entries = make([]Entry, p.Header.NumFiles)
	for i := range p.Entries {
		p.Entries[i].FromBuffer(b[RawEntrySize*i:])
	}

	return nil
}

func (p *Psarc) ParseBlockSizes() error {
	blockStorageLen := 2
	if p.Header.BlockSize > 0x10000 {
		blockStorageLen = 3
		if p.Header.BlockSize > 0x1000000 {
			blockStorageLen = 4
		}
	}

	sizesStartOffset := RawHeaderSize + RawEntrySize*int64(p.Header.NumFiles)
	sizesBufLen := int64(p.Header.TotalTocSize) - sizesStartOffset
	sizesCount := sizesBufLen / int64(blockStorageLen)

	b := make([]byte, sizesBufLen)
	if _, err := p.File.ReadAt(b, sizesStartOffset); err != nil {
		return fmt.Errorf("[psarc] BlockSizes ReadAt(..): %v", err)
	}

	p.BlockSizes = make([]uint32, sizesCount)
	for i := range p.BlockSizes {
		switch blockStorageLen {
		case 2:
			p.BlockSizes[i] = uint32(binary.BigEndian.Uint16(b[i*2:]))
		case 3:
			p.BlockSizes[i] = utils.Read24bitUint(binary.BigEndian, b[i*3:])
		case 4:
			p.BlockSizes[i] = binary.BigEndian.Uint32(b[i*4:])
		}
	}

	return nil
}

func (p *Psarc) ParseManifest() error {
	p.Entries[0].Name = "manifest"

	file := File{Entry: p.Entries[0], Psarc: p}
	if err := file.InitBuffer(); err != nil {
		return err
	}

	if rawManifest, err := ioutil.ReadAll(file.Buffer); err != nil {
		return err
	} else {
		b := bytes.NewBuffer(rawManifest)
		for i := 1; i < int(p.Header.NumFiles); i++ {
			name, _ := b.ReadString('\n')
			p.Entries[i].Name = strings.TrimSuffix(strings.TrimPrefix(name, "/"), "\n")
		}
	}

	return nil
}

func (p *Psarc) Unpack(outFolder string) error {
	if err := os.MkdirAll(outFolder, 0777); err != nil {
		return err
	}

	for i := range p.Entries {
		if i > 0 {
			log.Printf("[%.4d/%.4d] Unpacking %s\n", i, len(p.Entries)-1, p.Entries[i].Name)

			file := File{Entry: p.Entries[i], Psarc: p}
			if err := file.InitBuffer(); err != nil {
				log.Printf("\t%v", err)
			} else {
				if int64(file.Buffer.Len()) != p.Entries[i].OriginalSize {
					log.Printf("\tFile sizes not equals: %v != %v", file.Buffer.Len(), p.Entries[i].OriginalSize)
					file.Buffer = bytes.NewBuffer(file.Buffer.Bytes()[:p.Entries[i].OriginalSize])
				}
				path := filepath.Join(outFolder, p.Entries[i].Name)
				if err := os.MkdirAll(filepath.Dir(path), 0777); err != nil {
					return err
				}
				fof, err := os.Create(path)
				if err != nil {
					return err
				}
				//noinspection GoDeferInLoop, GoUnhandledErrorResult
				defer fof.Close()

				if _, err := file.Buffer.WriteTo(fof); err != nil {
					return err
				}
			}
		}
	}

	return nil
}

func NewPsarc(f *os.File) (*Psarc, error) {
	p := &Psarc{File: f}

	stat, err := os.Stat(f.Name())
	if err != nil {
		return nil, err
	}
	p.Reader = io.NewSectionReader(f, 0, stat.Size())

	if err := p.ParseHeader(); err != nil {
		return nil, err
	}
	if p.Header.CompressionMethod[0] != 0x7a {
		return nil, fmt.Errorf("only zlib compression supported (%#+v)", p.Header.CompressionMethod)
	}
	if err := p.ParseEntries(); err != nil {
		return nil, err
	}
	if err := p.ParseBlockSizes(); err != nil {
		return nil, err
	}
	if err := p.ParseManifest(); err != nil {
		return nil, err
	}

	return p, nil
}
