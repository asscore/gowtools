package toc

import (
	"encoding/binary"
	"errors"
	"io"
	"log"

	"gowtools/config"
	"gowtools/utils"
)

type RawTocEntry struct {
	Name   string
	Part   uint32
	Size   uint32
	Sector uint32
	Count  uint32
}

func DetectVersion(tocfile io.Reader) (config.GoWVersion, error) {
	buffer := make([]byte, 4)
	if _, err := tocfile.Read(buffer); err != nil {
		return config.GoWUnknown, err
	}

	var version config.GoWVersion = config.GoW1
	end := false

	for _, i := range buffer {
		if i == 0 {
			end = true
		} else if i < 20 || i > 127 {
			version = config.GoW2
		} else if end {
			version = config.GoW2
			break
		}
	}

	config.SetGoWVersion(version)

	return version, nil
}

func ParseGoW1(file io.Reader) ([]*RawTocEntry, error) {
	const EntrySize = 24

	buffer := make([]byte, EntrySize)
	temp := make(map[string]*RawTocEntry, 0)
	entries := make([]*RawTocEntry, 0)

	i := 0
	for {
		i++
		n, err := file.Read(buffer)
		if err != nil || n != EntrySize {
			if err == nil || err == io.EOF {
				return entries, nil
			} else {
				return nil, err
			}
		}

		entry := &RawTocEntry{
			Name:   utils.BytesToString(buffer[:12]),
			Part:   binary.LittleEndian.Uint32(buffer[12:16]),
			Size:   binary.LittleEndian.Uint32(buffer[16:20]),
			Sector: binary.LittleEndian.Uint32(buffer[20:24])}

		if _, ok := temp[entry.Name]; ok {
			temp[entry.Name].Count++
			if temp[entry.Name].Size != entry.Size {
				log.Printf("File is not copy %s\n", entry.Name)
			}
		} else {
			temp[entry.Name] = entry
			entries = append(entries, entry)
		}
	}
}

func ParseGoW2(file io.Reader) ([]*RawTocEntry, error) {
	const EntrySize = 36

	buffer := make([]byte, 4)

	if _, err := file.Read(buffer); err != nil {
		return nil, err
	}

	count := binary.LittleEndian.Uint32(buffer)
	maxIndex := uint32(0)

	buffer = make([]byte, EntrySize)
	temp := make(map[string]*RawTocEntry, 0)
	entries := make([]*RawTocEntry, 0)

	for i := uint32(0); i < count; i++ {
		if _, err := file.Read(buffer); err != nil {
			return nil, err
		}

		entry := &RawTocEntry{
			Name:   utils.BytesToString(buffer[:24]),
			Size:   binary.LittleEndian.Uint32(buffer[24:28]),
			Count:  binary.LittleEndian.Uint32(buffer[28:32]),
			Sector: binary.LittleEndian.Uint32(buffer[32:36])}

		if _, ok := temp[entry.Name]; ok {
			if temp[entry.Name].Size != entry.Size {
				log.Printf("File is not copy %s\n", entry.Name)
			}
		} else {
			temp[entry.Name] = entry
			entries = append(entries, entry)
		}

		if entry.Sector > maxIndex {
			maxIndex = entry.Sector
		}
	}

	buffer = make([]byte, 4)
	posMap := make([]uint32, maxIndex+1)

	for i := range posMap {
		if _, err := file.Read(buffer); err != nil {
			return nil, err
		}

		sz := binary.LittleEndian.Uint32(buffer)
		posMap[i] = sz
	}

	const DvdDlSplitline = 10000000

	for _, e := range entries {
		pos := posMap[e.Sector]
		e.Sector = pos % DvdDlSplitline
		e.Part = pos / DvdDlSplitline
	}

	return entries, nil
}

func Decode(tocFile io.ReadSeeker, version config.GoWVersion) ([]*RawTocEntry, error) {
	if version == config.GoWUnknown {
		var err error
		version, err = DetectVersion(tocFile)
		_, _ = tocFile.Seek(0, io.SeekStart)
		if err != nil {
			return nil, err
		}
		log.Printf("Detected TOC version: %v\n", version)
	}

	switch version {
	case config.GoW1:
		return ParseGoW1(tocFile)
	case config.GoW2:
		return ParseGoW2(tocFile)
	}

	return nil, errors.New("unknown TOC version for parsing")
}
