package vpk

import (
	"bytes"
	"encoding/binary"
	"errors"
	"io"
	"os"
	"path/filepath"

	"gowtools/libs/adpcm"
	"gowtools/utils"
)

// VPK - SCE America
type Vpk struct {
	SampleRate uint32
	Channels   uint32
	DataSize   uint32 // na kanał
}

func NewVpkFromReader(r io.ReaderAt) (*Vpk, error) {
	var header [0x20]byte
	if _, err := r.ReadAt(header[:], 0); err != nil {
		return nil, err
	}

	if bytes.Compare([]byte{0x20, 0x4b, 0x50, 0x56}, header[:0x4]) != 0 { // " KPV"
		return nil, errors.New("magic not matched")
	}

	vpk := &Vpk{
		DataSize:   binary.LittleEndian.Uint32(header[0x4:0x8]),
		SampleRate: binary.LittleEndian.Uint32(header[0x10:0x14]),
		Channels:   binary.LittleEndian.Uint32(header[0x14:0x18]),
	}

	// StartOffset - header[0x8:0xc] = 0x800
	// InterleaveBlockSize - header[0xc:0x10] = 0x2000

	return vpk, nil
}

func (vpk *Vpk) AsWave(r io.Reader, w io.Writer) (int, error) {
	if vpk.Channels > 4 {
		panic(vpk.Channels)
	}

	var inBlockSize = 0x1000 * vpk.Channels

	var in = make([]byte, inBlockSize)
	var out = make([]byte, adpcm.AdpcmSizeToWaveSize(int(inBlockSize)))

	if _, err := r.Read(in[:utils.SectorSize]); err != nil {
		return 0, err
	}

	var adpcmStream = make([]*adpcm.AdpcmStream, vpk.Channels)
	for i := range adpcmStream {
		adpcmStream[i] = adpcm.NewAdpcmStream()
	}

	if err := utils.WaveWriteHeader(w, uint16(vpk.Channels), vpk.SampleRate, uint32(adpcm.AdpcmSizeToWaveSize(int(vpk.DataSize))*2)); err != nil {
		return 0, err
	}

	inLeft := int(vpk.DataSize)
	n := 0
	for inLeft > 0 {
		if _, err := r.Read(in); err != nil {
			return 0, err
		}

		dataLen := 0x1000
		if inLeft < 0x1000 {
			dataLen = inLeft
		}

		for i, s := range adpcmStream {
			buf, err := s.Unpack(in[i*0x1000 : i*0x1000+dataLen])
			if err != nil {
				return 0, err
			}

			for k := 0; k < len(buf)/2; k++ {
				j := uint32(k * 2) // 16bit
				outPos := j*vpk.Channels + uint32(i*2)
				out[outPos] = buf[j]
				out[outPos+1] = buf[j+1]
			}
		}

		if wn, err := w.Write(out); err != nil {
			return n, err
		} else {
			n += wn
		}

		inLeft -= 0x1000
	}

	return n, nil
}

func WaveWrite(inFolder string, name string) (*Vpk, error) {
	vpkFile, err := os.Open(filepath.Join(inFolder, name))
	if err != nil {
		return nil, err
	}
	//noinspection GoUnhandledErrorResult
	defer vpkFile.Close()

	vpk, err := NewVpkFromReader(vpkFile)
	if err != nil {
		return nil, err
	}

	wavFile, err := os.Create(filepath.Join(inFolder, name+".wav"))
	if err != nil {
		return nil, err
	}
	//noinspection GoUnhandledErrorResult
	defer wavFile.Close()

	if _, err := vpk.AsWave(vpkFile, wavFile); err != nil {
		return nil, err
	}

	return vpk, nil
}
