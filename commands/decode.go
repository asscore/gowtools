package commands

import (
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os/exec"
	"path/filepath"
	"regexp"

	"gowtools/files/pss"
	"gowtools/files/vag"
	"gowtools/files/vpk"
)

type Decode struct {
	InFolder string
}

func (d *Decode) DefineFlags(f *flag.FlagSet) {
	f.StringVar(&d.InFolder, "in", "", "*Directory containing files to decode")
}

func (d *Decode) Run() error {
	if d.InFolder == "" {
		return errors.New("input folder argument required")
	}

	files, err := ioutil.ReadDir(d.InFolder)
	if err != nil {
		return err
	}

	vagRe := regexp.MustCompile(`(?i)^.*[.]VA[G1-5]$`)
	vpkRe := regexp.MustCompile(`(?i)^.*[.]VP[K1-5]$`)
	pssRe := regexp.MustCompile(`(?i)^.*[.]PS[SW]$`)
	atRe := regexp.MustCompile(`(?i)^.*[.]AT[39]$`)

	ext := make(map[string]string, 0)
	names := make([]string, 0)

	for _, f := range files {
		if vagRe.MatchString(f.Name()) {
			ext[f.Name()] = "VAG"
			names = append(names, f.Name())
		} else if vpkRe.MatchString(f.Name()) {
			ext[f.Name()] = "VPK"
			names = append(names, f.Name())
		} else if pssRe.MatchString(f.Name()) {
			ext[f.Name()] = "PSS"
			names = append(names, f.Name())
		} else if atRe.MatchString(f.Name()) {
			ext[f.Name()] = "ATRAC"
			names = append(names, f.Name())
		}
	}

	for i, name := range names {
		if ext[name] == "VAG" {
			if va, err := vag.WaveWrite(d.InFolder, name); err != nil {
				log.Printf("[%.4d/%.4d] %s\n\t%s\n", i+1, len(ext), name, err)
			} else {
				log.Printf("[%.4d/%.4d] (r:%vHz c:%v) %s\n", i+1, len(ext), va.SampleRate, va.Channels, name)
			}
		} else if ext[name] == "VPK" {
			if vp, err := vpk.WaveWrite(d.InFolder, name); err != nil {
				log.Printf("[%.4d/%.4d] %s\n\t%s\n", i+1, len(ext), name, err)
			} else {
				log.Printf("[%.4d/%.4d] (r:%vHz c:%v) %s\n", i+1, len(ext), vp.SampleRate, vp.Channels, name)
			}
		} else if ext[name] == "PSS" {
			if si, err := pss.Demux(d.InFolder, name); err != nil {
				log.Printf("[%.4d/%.4d] %s\n\t%s\n", i+1, len(ext), name, err)
			} else {
				log.Printf("[%.4d/%.4d] (r:%vHz c:%v s:%v) %s\n", i+1, len(ext), si.SampleRate, si.Channels,
					si.Count, name)
			}
		} else if ext[name] == "ATRAC" {
			tool := fmt.Sprintf("at%stool.exe", name[len(name)-1:])
			cmd := exec.Command(tool, "-d", filepath.Join(d.InFolder, name),
				filepath.Join(d.InFolder, name+".wav"))
			if err := cmd.Run(); err != nil {
				if err.Error() == "exit status 1" {
					err = fmt.Errorf("invalid file")
				}
				log.Printf("[%.4d/%.4d] %s\n\t%s\n", i+1, len(ext), name, err)
			} else {
				log.Printf("[%.4d/%.4d] %s\n", i+1, len(ext), name)
			}
		}
	}

	return nil
}
