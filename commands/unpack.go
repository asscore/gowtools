package commands

import (
	"errors"
	"flag"
	"log"
	"os"
	"path/filepath"

	"gowtools/config"
	"gowtools/files/iso"
	"gowtools/files/pak"
	"gowtools/files/psarc"
	"gowtools/files/toc"
)

type Unpack struct {
	GameFolder string
	IsoFile    string
	PsarcFile  string
	DoList     bool
	OutFolder  string
	Version    int
}

func (u *Unpack) DefineFlags(f *flag.FlagSet) {
	f.StringVar(&u.GameFolder, "in", "", "*Game folder (contains GODOFWAR.TOC file)")
	f.StringVar(&u.IsoFile, "iso", "", " ISO file")
	f.StringVar(&u.PsarcFile, "psarc", "", " PSARC file")
	f.BoolVar(&u.DoList, "l", false, " Not unpack, only list files in PAK archive")
	f.StringVar(&u.OutFolder, "out", "unpacked", " Directory to store result")
	f.IntVar(&u.Version, "v", 0, " Version of game: 0-auto; 1-GoW1; 2-GoW2")
}

func (u *Unpack) Run() error {
	config.SetGoWVersion(config.GoWVersion(u.Version))

	if u.PsarcFile != "" {
		psarcFile, err := os.Open(u.PsarcFile)
		if err != nil {
			return err
		}
		//noinspection GoUnhandledErrorResult
		defer psarcFile.Close()

		p, err := psarc.NewPsarc(psarcFile)
		if err != nil {
			return err
		}

		return p.Unpack(u.OutFolder)
	} else {
		if u.GameFolder == "" {
			return errors.New("game folder argument required")
		}

		if u.IsoFile != "" {
			if err := iso.Copy(u.IsoFile, u.GameFolder); err != nil {
				return err
			}
		}

		tocFile, err := os.Open(filepath.Join(u.GameFolder, "GODOFWAR.TOC"))
		if err != nil {
			return err
		}
		//noinspection GoUnhandledErrorResult
		defer tocFile.Close()

		entries, err := toc.Decode(tocFile, config.GetGoWVersion())
		if err != nil {
			return err
		}

		if u.DoList {
			for _, e := range entries {
				log.Printf("part: %d name: \"%s\" size: %d dups: %d", e.Part+1, e.Name, e.Size, e.Count)
			}
			return nil
		}

		return pak.Unpack(u.GameFolder, u.OutFolder, entries)
	}
}
