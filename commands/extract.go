package commands

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"gowtools/config"
	"gowtools/files/wad"
	_ "gowtools/files/wad/export"
)

type Extract struct {
	WadFile   string
	OutFolder string
	Print     bool
	Dump      bool
	PSVersion string
	Version   int
}

func (e *Extract) DefineFlags(f *flag.FlagSet) {
	f.StringVar(&e.WadFile, "wad", "", "*WAD file")
	f.StringVar(&e.OutFolder, "out", "extracted", " Directory to store result")
	f.BoolVar(&e.Print, "print", false, " Print user-friendly tree representation of WAD file")
	f.BoolVar(&e.Dump, "dump", false, " Dump all WAD nodes (.dump)")
	f.StringVar(&e.PSVersion, "ps", "ps2", " PlayStation version (ps2, ps3, psvita)")
	f.IntVar(&e.Version, "v", 0, " Version of game: 0-auto; 1-GoW1; 2-GoW2")
}

func (e *Extract) Run() error {
	if e.WadFile == "" {
		return errors.New("wad file argument required")
	}

	switch e.PSVersion {
	case "ps2":
		config.SetPSVersion(config.PS2)
	case "ps3":
		config.SetPSVersion(config.PS3)
	case "psvita":
		config.SetPSVersion(config.PSVita)
	default:
		log.Fatalf("Provide correct 'ps' parameter (ps2, ps3, psvita)")
	}

	config.SetGoWVersion(config.GoWVersion(e.Version))

	wadFile, err := os.Open(e.WadFile)
	if err != nil {
		return err
	}
	//noinspection GoUnhandledErrorResult
	defer wadFile.Close()

	wd, err := wad.NewWad(wadFile, filepath.Base(e.WadFile), config.GetGoWVersion())
	if err != nil {
		return err
	}

	if e.Print {
		for _, nd := range wd.Nodes {
			fmt.Println(nd)
		}
	}

	return wd.Extract(e.OutFolder, e.Dump)
}
