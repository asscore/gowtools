package adpcm

import (
	"errors"
	"io"
	"log"
)

// PlayStation 4bit ADPCM

var vagF = [5][2]float64{
	{0.0, 0.0},
	{60.0 / 64.0, 0.0},
	{115.0 / 64.0, -52.0 / 64.0},
	{98.0 / 64.0, -55.0 / 64.0},
	{122.0 / 64.0, -60.0 / 64.0},
}

//noinspection GoNameStartsWithPackageName
type AdpcmStream struct {
	hist1 float64
	hist2 float64
}

//noinspection GoNameStartsWithPackageName
func AdpcmSizeToWaveSize(size int) int { // 4bit ADPCM
	return (size / 16) * 28 * 2 // 16bit PCM
}

func (stream *AdpcmStream) Unpack(packs []byte) ([]byte, error) {
	if len(packs)%16 != 0 {
		return nil, errors.New("support only 8 bytes blocks of stream")
	}

	var result = make([]byte, AdpcmSizeToWaveSize(len(packs)))
	var fSamples [28]float64

	iResultPos := 0
	for iBlock := 0; iBlock < len(packs)/16; iBlock++ {
		blockStart := iBlock * 16

		if packs[blockStart] == 0xc0 {
			continue
		}

		predictNr := uint32(packs[blockStart])
		shiftFactor := predictNr & 0xf
		predictNr >>= 4

		if predictNr > 4 {
			log.Printf("\tStrange sound: p:%2d b:%v", predictNr, packs[blockStart:blockStart+16])
			predictNr = 0
		}

		streamPos := blockStart + 2

		for i := 0; i < 28; i += 2 {
			sample := uint32(packs[streamPos])
			streamPos++

			scale := int16(sample&0xf) << 12
			fSamples[i] = float64(scale >> shiftFactor)

			scale = int16(sample&0xf0) << 8
			fSamples[i+1] = float64(scale >> shiftFactor)
		}

		bSamples := result[iResultPos:]
		for i := range fSamples {
			fSamples[i] = fSamples[i] + stream.hist1*vagF[predictNr][0] + stream.hist2*vagF[predictNr][1]
			stream.hist2 = stream.hist1
			stream.hist1 = fSamples[i]
			d := int(fSamples[i] + 0.5)
			bSamples[i*2] = byte(d & 0xff)
			bSamples[i*2+1] = byte(d >> 8)
		}
		iResultPos += 56
	}

	return result[:iResultPos], nil
}

func NewAdpcmStream() *AdpcmStream {
	return &AdpcmStream{}
}

//noinspection GoNameStartsWithPackageName
type AdpcmToWaveStream struct {
	outStream io.Writer
	AdpcmStream
}

func NewAdpcmToWaveStream(waveWriterOutput io.Writer) *AdpcmToWaveStream {
	return &AdpcmToWaveStream{outStream: waveWriterOutput}
}

func (stream *AdpcmToWaveStream) Write(p []byte) (n int, err error) {
	if len(p)%16 != 0 {
		return 0, errors.New("support only 8 bytes blocks of stream")
	}

	u, err := stream.Unpack(p)
	if err != nil {
		return 0, err
	}

	return stream.outStream.Write(u)
}
