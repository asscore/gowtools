
  ▄████  ▒█████  ▓█████▄  ▒█████    █████▒█     █░ ▄▄▄       ██▀███
 ██▒ ▀█▒▒██▒  ██▒▒██▀ ██▌▒██▒  ██▒▓██   ▒▓█░ █ ░█░▒████▄    ▓██ ▒ ██▒
▒██░▄▄▄░▒██░  ██▒░██   █▌▒██░  ██▒▒████ ░▒█░ █ ░█ ▒██  ▀█▄  ▓██ ░▄█ ▒
░▓█  ██▓▒██   ██░░▓█▄   ▌▒██   ██░░▓█▒  ░░█░ █ ░█ ░██▄▄▄▄██ ▒██▀▀█▄
░▒▓███▀▒░ ████▓▒░░▒████▓ ░ ████▓▒░░▒█░   ░░██▒██▓  ▓█   ▓██▒░██▓ ▒██▒
 ░▒   ▒ ░ ▒░▒░▒░  ▒▒▓  ▒ ░ ▒░▒░▒░  ▒ ░   ░ ▓░▒ ▒   ▒▒   ▓▒█░░ ▒▓ ░▒▓░
  ░   ░   ░ ▒ ▒░  ░ ▒  ▒   ░ ▒ ▒░  ░       ▒ ░ ░    ▒   ▒▒ ░  ░▒ ░ ▒░
      ░     ░ ░     ░        ░ ░             ░          ░  ░   ░

GoWTools - aplikacja wspomagająca analizę struktury plików gry God of War (PS2).

Autor: ProMax/ASSCoRE (promax@asscore.pl)

= Unpack ===
Wypakowanie zawartości plików PART*.PAK przy użyciu informacji z GODOFWAR.TOC.

Wywołanie: ./gowtools.exe unpack -in <katalog z GODOFWAR.TOC oraz PART*.PAK>
Pomoc: ./gowtools.exe unpack --help

Formaty w pliku PART*.PAK:
.-----------.-----------------------------------------.
| Format    | Opis                                    |
.-----------+-----------------------------------------.
| PSS/PSW   | pliki MPEG2 video (PSW - widescreen)    |
| WAD       | pliki archiwum                          |
| VAG/VA1-5 | pliki dźwiękowe VAGp 4bit ADPCM         |
| VPK/VP1-5 | pliki dźwiękowe RAW 4bit ADPCM          |
| TXT       | SANITY.TXT służy do weryfikacji         |
`-----------`-----------------------------------------`

Wersje językowe VA*/VP* i ścieżki audio w PSS/PSW (tylko wersja PAL):
1 - francuski
2 - niemiecki
3 - włoski
4 - hiszpański
5 - rosyjski (tylko druga część)

= Decode ===
Konwersja plików VAG/VPK do WAV i PSS/PSW do M2V i WAV.

Wywołanie: ./gowtools.exe decode -in <katalog z plikami do przetworzenia>
Pomoc: ./gowtools.exe decode --help

= Extract ==
Eksport tekstur do PNG i obiektów/modeli do OBJ i MTL z pliku WAD.

Wywołanie: ./gowtools.exe extract -wad <plik wad>
Pomoc: ./gowtools.exe extract --help
