package utils

import (
	"encoding/binary"
	"io"
)

func WaveWriteHeader(w io.Writer, channels uint16, sampleRate uint32, dataSize uint32) error {
	var buf [0x2c]byte
	var pos = 0

	write16 := func(v uint16) {
		binary.LittleEndian.PutUint16(buf[pos:pos+2], v)
		pos += 2
	}

	write32 := func(v uint32) {
		binary.LittleEndian.PutUint32(buf[pos:pos+4], v)
		pos += 4
	}

	write32(0x46464952)                        // ChunkID ("RIFF")
	write32(36 + dataSize)                     // ChunkSize (36 + SubChunk2Size)
	write32(0x45564157)                        // Format ("WAVE")
	write32(0x20746d66)                        // Subchunk1ID ("fmt ")
	write32(16)                                // Subchunk1Size (16 = PCM)
	write16(1)                                 // AudioFormat (1 = PCM)
	write16(channels)                          // NumChannels (1 = mono, 2 = stereo, ...)
	write32(sampleRate)                        // SampleRate (8000, 44100, ...)
	write32(sampleRate * uint32(channels) * 2) // ByteRate (SampleRate * NumChannels * BitsPerSample/8)
	write16(channels * 2)                      // BlockAlign (NumChannels * BitsPerSample/8)
	write16(16)                                // BitsPerSample (16 = 16bit)
	write32(0x61746164)                        // Subchunk2ID ("data")
	write32(dataSize)                          // Subchunk2Size (NumSamples * NumChannels * BitsPerSample/8)

	_, err := w.Write(buf[:]) // Data
	return err
}
