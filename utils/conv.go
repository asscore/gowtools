package utils

import (
	"bytes"
	"encoding/binary"
)

const SectorSize = 0x800

func BytesToString(bs []byte) string {
	n := bytes.IndexByte(bs, 0)
	if n < 0 {
		n = len(bs)
	}
	return string(bs[0:n])
}

func Read24bitUint(o binary.ByteOrder, bin []byte) uint32 {
	var buf [4]byte
	if o == binary.LittleEndian {
		copy(buf[0:], bin[:3])
	} else {
		copy(buf[1:], bin[:3])
	}

	return o.Uint32(buf[:])
}

func Read40bitUint(o binary.ByteOrder, bin []byte) uint64 {
	var buf [8]byte
	if o == binary.LittleEndian {
		copy(buf[0:], bin[:5])
	} else {
		copy(buf[3:], bin[:5])
	}

	return o.Uint64(buf[:])
}
