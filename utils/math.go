package utils

type Mat4 [16]float32
type Vec3 [3]float32
type Vec4 [4]float32

func Ident4() Mat4 {
	return Mat4{1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1}
}

func (m4 Mat4) Mul4(m Mat4) Mat4 {
	return Mat4{
		m4[0]*m[0] + m4[4]*m[1] + m4[8]*m[2] + m4[12]*m[3],
		m4[1]*m[0] + m4[5]*m[1] + m4[9]*m[2] + m4[13]*m[3],
		m4[2]*m[0] + m4[6]*m[1] + m4[10]*m[2] + m4[14]*m[3],
		m4[3]*m[0] + m4[7]*m[1] + m4[11]*m[2] + m4[15]*m[3],
		m4[0]*m[4] + m4[4]*m[5] + m4[8]*m[6] + m4[12]*m[7],
		m4[1]*m[4] + m4[5]*m[5] + m4[9]*m[6] + m4[13]*m[7],
		m4[2]*m[4] + m4[6]*m[5] + m4[10]*m[6] + m4[14]*m[7],
		m4[3]*m[4] + m4[7]*m[5] + m4[11]*m[6] + m4[15]*m[7],
		m4[0]*m[8] + m4[4]*m[9] + m4[8]*m[10] + m4[12]*m[11],
		m4[1]*m[8] + m4[5]*m[9] + m4[9]*m[10] + m4[13]*m[11],
		m4[2]*m[8] + m4[6]*m[9] + m4[10]*m[10] + m4[14]*m[11],
		m4[3]*m[8] + m4[7]*m[9] + m4[11]*m[10] + m4[15]*m[11],
		m4[0]*m[12] + m4[4]*m[13] + m4[8]*m[14] + m4[12]*m[15],
		m4[1]*m[12] + m4[5]*m[13] + m4[9]*m[14] + m4[13]*m[15],
		m4[2]*m[12] + m4[6]*m[13] + m4[10]*m[14] + m4[14]*m[15],
		m4[3]*m[12] + m4[7]*m[13] + m4[11]*m[14] + m4[15]*m[15],
	}
}

func (m4 Mat4) Mul4x1(v Vec4) Vec4 {
	return Vec4{
		m4[0]*v[0] + m4[4]*v[1] + m4[8]*v[2] + m4[12]*v[3],
		m4[1]*v[0] + m4[5]*v[1] + m4[9]*v[2] + m4[13]*v[3],
		m4[2]*v[0] + m4[6]*v[1] + m4[10]*v[2] + m4[14]*v[3],
		m4[3]*v[0] + m4[7]*v[1] + m4[11]*v[2] + m4[15]*v[3],
	}
}

func (v3 Vec3) Vec4(f float32) Vec4 {
	return Vec4{v3[0], v3[1], v3[2], f}
}

func (v4 Vec4) Vec3() Vec3 {
	return Vec3{v4[0], v4[1], v4[2]}
}

func (v4 Vec4) Mul(f float32) Vec4 {
	return Vec4{v4[0] * f, v4[1] * f, v4[2] * f, v4[3] * f}
}

func TransformCoordinate(v Vec3, m Mat4) Vec3 {
	t := v.Vec4(1)
	t = m.Mul4x1(t)
	t = t.Mul(1 / t[3])

	return t.Vec3()
}
