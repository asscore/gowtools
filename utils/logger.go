package utils

import (
	"fmt"
	"io"
)

type Logger struct {
	Writer io.Writer
}

func (l *Logger) Write(p []byte) (n int, err error) {
	return l.Writer.Write(p)
}

func (l *Logger) Println(a ...interface{}) {
	if l != nil {
		//noinspection GoUnhandledErrorResult
		fmt.Fprintln(l.Writer, a...)
	}
}

func (l *Logger) Printf(format string, a ...interface{}) {
	if l != nil {
		//noinspection GoUnhandledErrorResult
		fmt.Fprintf(l.Writer, format+"\n", a...)
	}
}
