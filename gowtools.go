package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"gowtools/commands"
)

type Command interface {
	DefineFlags(*flag.FlagSet)
	Run() error
}

var cmds = map[string]Command{
	"unpack":  &commands.Unpack{},
	"decode":  &commands.Decode{},
	"extract": &commands.Extract{},
}

func main() {
	flag.Usage = func() {
		fmt.Println("Usage: gowtools command [arguments]")
		fmt.Println("Help: gowtools command --help")
		fmt.Println("Commands:")
		for i := range cmds {
			fmt.Printf("  %s\n", i)
		}
	}

	flag.Parse()

	if flag.NArg() < 1 {
		flag.Usage()
		os.Exit(0)
	}

	cmdName := flag.Arg(0)
	if sc, ok := cmds[cmdName]; ok {
		fs := flag.NewFlagSet(cmdName, flag.ExitOnError)
		sc.DefineFlags(fs)
		//noinspection GoUnhandledErrorResult
		fs.Parse(flag.Args()[1:])

		if err := sc.Run(); err != nil {
			log.Printf("Program exit with error: %v\n", err)
			os.Exit(2)
		}
	} else {
		log.Printf("%s is not a valid command", cmdName)
		flag.Usage()
		os.Exit(1)
	}
}
